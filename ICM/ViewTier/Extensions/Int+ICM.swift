//
//  Int+ICM.swift
//  ICM
//
//  Created by Mikael Son on 2/18/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

extension Int {
    
    func toStringMonth() -> String {
        var string = ""
        switch self {
        case 1:
            string = "January"
        case 2:
            string = "Februrary"
        case 3:
            string = "March"
        case 4:
            string = "April"
        case 5:
            string = "May"
        case 6:
            string = "June"
        case 7:
            string = "July"
        case 8:
            string = "August"
        case 9:
            string = "September"
        case 10:
            string = "October"
        case 11:
            string = "November"
        case 12:
            string = "December"
        default:
            break
        }
        return string
    }
}
