//
//  String+ICM.swift
//  ICM
//
//  Created by Mikael Son on 2/18/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

extension String {
    
    func toDate() -> Date {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy  hh:mm:ss a"
        
        if let date = dateFormatter.date(from: self) {
            return date
        } else {
            return Date()
        }
    }
}
