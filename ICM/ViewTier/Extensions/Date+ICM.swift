//
//  Date+ICM.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

extension Date {
    
    static func getCurrentTime() -> Date {
        return Date()
    }
    
    func toString() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy  hh:mm:ss a"
        return dateFormatter.string(from: self)
    }
}
