//
//  UIColor+ICM.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

extension UIColor {
    
    struct ICM {
        static let black = UIColor.rgb(red: 46, green: 46, blue: 46)
        static let blue = UIColor.rgb(red: 80, green: 145, blue: 205)
        static let red = UIColor.rgb(red: 244, green: 67, blue: 33)
        static let green = UIColor.rgb(red: 122, green: 193, blue: 67)
        static let orange = UIColor.rgb(red: 249, green: 165, blue: 65)
        static let gray = UIColor.rgb(red: 101, green: 119, blue: 130)
        static let lightGray = UIColor.rgb(red: 231, green: 235, blue: 240)
        static let cloudyBlue = UIColor.rgb(red: 214, green: 218, blue: 228)
    }
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
}
