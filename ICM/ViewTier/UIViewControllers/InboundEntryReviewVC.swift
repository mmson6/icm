//
//  InboundEntryReviewVC.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol InboundEntryReviewVCDelegate: class {
    func backgroundTappedToDismiss()
    func submitButtonTapped(with dataModel: InboundEntryModel)
}

public class InboundEntryReviewVC: UIViewController {

    weak var delegate: InboundEntryReviewVCDelegate?
    
    @IBOutlet private weak var fromLabel: UILabel!
    @IBOutlet private weak var toLabel: UILabel!
    @IBOutlet private weak var palletCountLabel: UILabel!
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    
    public var dataModel: InboundEntryModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.loadData()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        self.cancelButton.layer.cornerRadius = self.cancelButton.frame.height / 2
        self.cancelButton.layer.borderColor = self.cancelButton.currentTitleColor.cgColor
        self.cancelButton.layer.borderWidth = 1
        
        self.submitButton.tintColor = .white
        self.submitButton.layer.cornerRadius = self.submitButton.frame.height / 2
        self.submitButton.backgroundColor = UIColor.ICM.orange
    }
    
    private func loadData() {
        guard let model = self.dataModel else { return }
        
        self.fromLabel.text = model.fromValue
        self.toLabel.text = model.toValue
        self.palletCountLabel.text = model.palletCountValue
        self.weightLabel.text = model.weightValue
        self.timeLabel.text = model.timeValue
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        if let dataModel = self.dataModel {
            self.delegate?.submitButtonTapped(with: dataModel)
        }
    }
}
