//
//  AddRackVC.swift
//  ICM
//
//  Created by Mikael Son on 2/10/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol AddRackVCDelegate: class {
    func backgroundTappedToDismiss()
    func addButtonTapped(with dataModel: AddRackModel, sender: AddRackVC)
}

public class AddRackVC: UIViewController {

    weak var delegate: AddRackVCDelegate?
    
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var infoTextField: UITextField!
    @IBOutlet private weak var addButton: UIButton!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        
        self.nameTextField.becomeFirstResponder()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        self.addButton.backgroundColor = UIColor.ICM.orange
    self.addButton.setTitleColor(.white, for: .normal)
        self.addButton.layer.cornerRadius = self.addButton.frame.height / 2
        
        self.disableAddButton()
    }
    
    private func enableAddButton() {
        self.addButton.alpha = 1
        self.addButton.isUserInteractionEnabled = true
    }
    
    private func disableAddButton() {
        self.addButton.alpha = 0.5
        self.addButton.isUserInteractionEnabled = false
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func addButtonTapped(_ sender: Any) {
        guard
            let name = self.nameTextField.text,
            let info = self.infoTextField.text
        else { return }
        
        let dataModel = AddRackModel(name: name.lowercased(), info: info)
        self.delegate?.addButtonTapped(with: dataModel, sender: self)
    }
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    // Mark: - UITextField
    
    @IBAction func textFiedDidChange(_ sender: UITextField) {
        if self.nameTextField.text != "" {
            self.enableAddButton()
        } else {
            self.disableAddButton()
        }
    }
    
}
