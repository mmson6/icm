//
//  PalletEntryVC.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol PalletEntryVCDelegate: class {
    func palletEntryCloseButtonTapped(sender: UIViewController)
    func palletEntrySubmitted(sender: UIViewController)
}

public class PalletEntryVC: UIViewController, SetDateAndTimeVCDelegate, PalletEntryReviewVCDelegate, UITextFieldDelegate {

    weak var delegate: PalletEntryVCDelegate?
    
    @IBOutlet public weak var palletNumbTextField: UITextField!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet private weak var setDateAndTimeButton: UIButton!
    
    private var presentingWindowRef: UIWindow?
    public var editingTextFieldRef: UITextField?
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        let image = UIImage(named: "close")?.withRenderingMode(.alwaysTemplate)
        self.closeButton.setImage(image, for: .normal)
        self.closeButton.tintColor = UIColor.rgb(red: 46, green: 46, blue: 46)
        
        self.setDateAndTimeButton.layer.borderWidth = 1
        self.setDateAndTimeButton.layer.cornerRadius = 5
        self.setDateAndTimeButton.layer.borderColor = UIColor.ICM.blue.cgColor
        
        self.submitButton.tintColor = .white
        self.submitButton.layer.cornerRadius = self.submitButton.frame.height / 2
        self.submitButton.backgroundColor = UIColor.ICM.green
        
        self.disableSubmitButton()
    }
    
    private func disableSubmitButton() {
        self.submitButton.alpha = 0.5
        self.submitButton.isUserInteractionEnabled = false
    }
    
    private func enableSubmitButton() {
        self.submitButton.alpha = 1
        self.submitButton.isUserInteractionEnabled = true
    }
    
    private func dismissOverlayWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingWindowRef?.alpha = 0
        }) { (success) in
            self.presentingWindowRef = nil
        }
    }
    
    private func validate() -> Bool {
        return true
    }
    
    private func showPalletEntryConfirmationView(for dataModel: PalletEntryModel) {
        let storyboard = UIStoryboard(name: "Entry", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "PalletEntryReviewVC") as? PalletEntryReviewVC
        else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        vc.dataModel = dataModel
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3) {
            window.alpha = 1
        }
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func setDateAndTimeButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Entry", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "SetDateAndTimeVC") as? SetDateAndTimeVC
        else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3) {
            window.alpha = 1
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.delegate?.palletEntryCloseButtonTapped(sender: self)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        if self.validate() {
            guard
                let pallet = self.palletNumbTextField.text
            else { return }
            
            let dataModel = PalletEntryModel(
                pallet: pallet,
                time: "none")
            
            self.showPalletEntryConfirmationView(for: dataModel)
        }
    }
    
    public func backgroundTappedToDismiss() {
        self.dismissOverlayWindow()
    }
    
    // MARK: - PalletEntryReviewVCDelegate Functions
    
    public func palletEntrySubmitted() {
        self.editingTextFieldRef?.resignFirstResponder()
        self.delegate?.palletEntrySubmitted(sender: self)
    }
    
    // MARK: - UITextFieldDelegate Functions
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.editingTextFieldRef = textField
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        
        if self.palletNumbTextField.text != "" {
            self.enableSubmitButton()
        } else {
            self.disableSubmitButton()
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
