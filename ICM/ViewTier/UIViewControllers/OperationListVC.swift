//
//  OperationListVC.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class OperationListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet private var tableView: UITableView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    // MARK: - Helper Functions

    private func layoutViews() {
        self.tableView.separatorStyle = .none
    }

    // MARK: - UITableViewDelegate Functions

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "InboundCell", for: indexPath)

        guard
            let cell = safeCell as? InboundCell
        else { return safeCell }

        // Mocked Data
        cell.containerView.layer.borderWidth = 2
        if indexPath.row > 1 {
            cell.containerView.layer.borderColor = UIColor.ICM.green.cgColor
        } else if indexPath.row == 1 {
            cell.containerView.layer.borderColor = UIColor.ICM.orange.cgColor
        } else if indexPath.row == 0 {
            cell.containerView.layer.borderColor = UIColor.ICM.blue.cgColor
        }
        
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
