//
//  SettingsTVC.swift
//  ICM
//
//  Created by Mikael Son on 2/9/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

fileprivate let sectionZeroCellCount = 1
fileprivate let sectionOneCellCount = 1
fileprivate let sectionTwoCellCount = 2
fileprivate let sectionThreeCellCount = 1


public class SettingsTVC: UITableViewController {

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
    }
    
    private func settingOptionCell(with indexPath: IndexPath) -> UITableViewCell {
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "SettingOptionCell", for: indexPath)
        
        guard
            let cell = safeCell as? SettingOptionCell
            else { return safeCell }
        
        switch indexPath.section {
        case 0:
            cell.label.text = "ICM - Innovative Cargo Manager"
            cell.isUserInteractionEnabled = false
        case 1:
            cell.label.text = "Manage Racks"
            cell.accessoryType = .disclosureIndicator
        case 2:
            if indexPath.row == 1 {
                cell.label.text = "Privacy Policy"
                cell.accessoryType = .disclosureIndicator
            } else if indexPath.row == 2 {
                cell.label.text = "Open Source Libraries"
                cell.accessoryType = .disclosureIndicator
            }
        case 3:
            cell.label.text = "Log Out"
            cell.label.textColor = UIColor.ICM.blue
        default:
            return safeCell
        }
        
        return cell
    }
    
    private func settingSectionHeaderCell(with indexPath: IndexPath) -> UITableViewCell {

        let safeCell = tableView.dequeueReusableCell(withIdentifier: "SettingSectionHeaderCell", for: indexPath)
        
        guard
            let cell = safeCell as? SettingSectionHeaderCell
        else { return safeCell }
        
        switch indexPath.section {
        case 0:
            cell.titleLabel.text = "PRODUCT"
        case 1:
            cell.titleLabel.text = "INBOUND"
        case 2:
            cell.titleLabel.text = "ABOUT"
        case 3:
            cell.titleLabel.text = "ACCOUNT"
        default:
            break
        }
        
        return cell
    }
    
    // MARK: - TableViewController Functions

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return sectionZeroCellCount + 1
        case 1:
            return sectionOneCellCount + 1
        case 2:
            return sectionTwoCellCount + 1
        case 3:
            return sectionThreeCellCount + 1
        default:
            return 0
        }
    }

    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
        header.backgroundColor = UIColor.ICM.cloudyBlue
        return header
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
        footer.backgroundColor = UIColor.ICM.cloudyBlue
        return footer
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 3 ? 1 : 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return self.settingSectionHeaderCell(with: indexPath)
        } else {
            return settingOptionCell(with: indexPath)
        }
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1 {
            self.performSegue(withIdentifier: "showManageRacks", sender: nil)
        } else if indexPath.section == 2 {
            if indexPath.row == 2 {
                self.performSegue(withIdentifier: "showAcknowledgement", sender: nil)
            }
        }
    }
    
    // MARK: - Navigation
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == "showAcknowledgement" {
            guard
                let destination = segue.destination as? AcknowledgementVC
            else { return }
            
            destination.hidesBottomBarWhenPushed = true
        }
    }
}
