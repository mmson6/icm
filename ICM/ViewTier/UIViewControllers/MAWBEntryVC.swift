//
//  MAWBEntryVC.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol MAWBEntryVCDelegate: class {
    func billNumberEntryCloseButtonTapped(sender: UIViewController)
    func MAWBEntrySubmitted(sender: UIViewController)
}

public class MAWBEntryVC: UIViewController, AssignRackVCDelegate, SetDateAndTimeVCDelegate, MAWBEntryReviewVCDelegate, UITextFieldDelegate {
    
    weak var delegate: MAWBEntryVCDelegate?
    
    @IBOutlet private weak var createTimeLabel: UILabel!
    @IBOutlet private weak var MAWBTextField: UITextField!
    @IBOutlet private weak var freightCountTextField: UITextField!
    @IBOutlet public weak var palletNumbTextField: UITextField!
    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var assignRackButton: UIButton!
    @IBOutlet private weak var rackAssignmentLabel: UILabel!
    
    private var presentingWindowRef: UIWindow?
    public var editingTextFieldRef: UITextField?
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.initializeCreateTime()
    }
    
    // MARK: - Commands
    
    private func submitMAWBEntry(with dataModel: MAWBEntryModel) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        let cmd = SubmitMAWBEntryCommand()
        cmd.execute(dataModel: dataModel).then { (result) in
            switch result {
            case .success(let success):
                if success {
                    NSLog("MAWB Entry Submitted Successfully")
                    self.MAWBEntrySubmitted()
                } else {
                    NSLog("MAWB Entry Submission failed")
                }
            case .failure(let error):
                NSLog("Failed to submit MAWB Entry : \(error)")
                self.showRequestFailedAlert()
            }
            FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
        }
    }
    
    // MARK: - Helper Functions
    
    private func disableSubmitButton() {
        self.submitButton.alpha = 0.5
        self.submitButton.isUserInteractionEnabled = false
    }
    
    private func dismissOverlayWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingWindowRef?.alpha = 0
        }) { (success) in
            self.presentingWindowRef = nil
        }
    }
    
    private func enableSubmitButton() {
        self.submitButton.alpha = 1
        self.submitButton.isUserInteractionEnabled = true
    }
    
    private func initializeCreateTime() {
        self.createTimeLabel.text = Date.getCurrentTime().toString()
    }
    
    private func layoutViews() {
        let image = UIImage(named: "close")?.withRenderingMode(.alwaysTemplate)
        self.closeButton.setImage(image, for: .normal)
        self.closeButton.tintColor = UIColor.rgb(red: 46, green: 46, blue: 46)
        
        self.assignRackButton.layer.borderWidth = 1
        self.assignRackButton.layer.cornerRadius = 5
        self.assignRackButton.layer.borderColor = UIColor.ICM.blue.cgColor
        
        self.submitButton.tintColor = .white
        self.submitButton.layer.cornerRadius = self.submitButton.frame.height / 2
        self.submitButton.backgroundColor = UIColor.ICM.orange
        
        self.disableSubmitButton()
    }
    
    private func showMAWBEntryConfirmationView(for dataModel: MAWBEntryModel) {
        let storyboard = UIStoryboard(name: "Entry", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "MAWBEntryReviewVC") as? MAWBEntryReviewVC
            else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        vc.dataModel = dataModel
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3) {
            window.alpha = 1
        }
    }
    
    private func showRequestFailedAlert() {
        let alertController = UIAlertController(
            title: "Submission Failed",
            message: "Error occurred while submitting entry. Please try again.",
            preferredStyle: .alert
        )
        
        let okayAction = UIAlertAction(
            title: "Okay",
            style: .default,
            handler: nil
        )
        
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func validate() -> Bool {
        return true
    }
    
    private func updateSubmitButton() {
        if self.palletNumbTextField.text != "" && self.MAWBTextField.text != "" &&
            self.freightCountTextField.text != "" && self.rackAssignmentLabel.text != "Not Assigned" {
            self.enableSubmitButton()
        } else {
            self.disableSubmitButton()
        }
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func assignRackButtonTapped(_ sender: Any) {
        self.editingTextFieldRef?.resignFirstResponder()
        
        let storyboard = UIStoryboard(name: "Entry", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "AssignRackVC") as? AssignRackVC
        else { return }
        
        vc.delegate = self
        
        let window = UIWindow()
        self.presentingWindowRef = window
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3) {
            window.alpha = 1
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.delegate?.billNumberEntryCloseButtonTapped(sender: self)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        if self.validate() {
            guard
                let parent = self.parent as? EntryCoordinatorVC,
                let inboundID = parent.inboundID,
                let pallet = self.palletNumbTextField.text,
                let MAWB = self.MAWBTextField.text,
                let freightCountString = self.freightCountTextField.text,
                let freightCount = Int(freightCountString),
                let rack = self.rackAssignmentLabel.text,
                let time = self.createTimeLabel.text
            else { return }
            
            
            let dataModel = MAWBEntryModel(
                inboundID: inboundID,
                pallet: pallet.lowercased(),
                MAWB: MAWB.lowercased(),
                freightCount: freightCount,
                rack: rack,
                time: time)
            
            self.showMAWBEntryConfirmationView(for: dataModel)
        }
    }
    
    // MARK: - AssignRackVCDelegate Functions
    
    public func rackSelectedForAssignment(with screenModel: RackScreenModel) {
        self.rackAssignmentLabel.text = screenModel.nameValue
        self.updateSubmitButton()
        self.dismissOverlayWindow()
    }
    
    // MARK: - MAWBEntryReviewVCDelegate Functions
    
    public func submitButtonTapped(with dataModel: MAWBEntryModel) {
        self.dismissOverlayWindow()
        self.submitMAWBEntry(with: dataModel)
    }
    
    public func submitButtonTapped(with dataModel: AddRackModel, sender:
        AddRackVC) {
        
    }
    
    public func MAWBEntrySubmitted() {
        self.editingTextFieldRef?.resignFirstResponder()
        self.delegate?.MAWBEntrySubmitted(sender: self)
    }
    
    // MARK: - Overlaying UIWindow Dismiss Function
    
    public func backgroundTappedToDismiss() {
        self.dismissOverlayWindow()
    }
    
    // MARK: - UITextFieldDelegate Functions
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.editingTextFieldRef = textField
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        self.updateSubmitButton()
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 10 {
            self.MAWBTextField.becomeFirstResponder()
        } else if textField.tag == 11 {
            self.freightCountTextField.becomeFirstResponder()
        } else if textField.tag == 12 {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
