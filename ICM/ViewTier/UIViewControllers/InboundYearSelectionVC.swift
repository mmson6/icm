//
//  InboundYearSelectionVC.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol InboundYearSelectionVCDelegate: class {
    func backgroundTappedToDismiss()
    func yearSelected(with year: String)
}

fileprivate let sectionInsets = UIEdgeInsets(top: 40.0, left: 10.0, bottom: 40.0, right: 10.0)
fileprivate let itemsPerRow: CGFloat = 5


public class InboundYearSelectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    weak var delegate: InboundYearSelectionVCDelegate?
    
    private var dataSource = [String]()
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.loadDataSource()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {}
    
    private func loadDataSource() {
        let date = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: date)
        
        var yearsSince = [String]()
        for year in 2018...2020 {
            yearsSince.append("\(year)")
        }
        self.dataSource = yearsSince
        self.collectionView.reloadData()
    }
    
    // MARK: - UICollectionViewDelegate Functions
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let safeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "YearCVCell", for: indexPath)
        
        let year = self.dataSource[indexPath.row]
        guard
            let cell = safeCell as? YearCVCell
        else { return safeCell }
        
        cell.year = year
        cell.yearLabel.text = year
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard
            let cell = collectionView.cellForItem(at: indexPath) as? YearCVCell,
            let year = cell.year
        else { return }
        
        self.delegate?.yearSelected(with: year)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout Functions
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
}
