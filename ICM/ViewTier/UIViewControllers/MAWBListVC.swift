//
//  MAWBListVC.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit


public class MAWBListVC: SPRTableViewController, UISearchResultsUpdating, UISearchBarDelegate {

    @IBOutlet private var noEntryContainer: UIView!
    
    public var inboundID: String?
    private var refreshController = UIRefreshControl()
    private var searchController = UISearchController()
    private var searchScope: Int = 0
    private var isSearching: Bool = false
    private var dataSource = [MAWBScreenModel]()
    private var searchDataSource = [MAWBScreenModel]()
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.initRefreshControl()
        self.initializeSearchController()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchController.searchBar.resignFirstResponder()
    }
    
    // MARK: - Commands
    
    private func readMAWBs(delay: Double = 0) -> FutureResult<ObjectDataSource<Any>> {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        let deferred = DeferredResult<ObjectDataSource<Any>>()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            let cmd = ReadMAWBsByInboundCommand()
            cmd.execute(with: self.inboundID).then { (result) in
                switch result {
                case .success(let screenModels):
                    NSLog("ReadMAWBsByInboundCommand returned with success")
                    if screenModels.count == 0 {
                        self.noEntryContainer.isHidden = false
                        self.tableView.separatorStyle = .none
                    } else {
                        self.noEntryContainer.isHidden = true
                        self.tableView.separatorStyle = .singleLine
                    }
                    self.dataSource = screenModels
                    if delay != 0 && self.dataSource.count != 0 {
                        self.updateSearchResults(for: self.searchController)
                    }
                    let dataSource = ArrayObjectDataSource(objects: screenModels)
                    deferred.success(value: dataSource.asAny())
                case .failure(let error):
                    NSLog("ReadMAWBsByInboundCommand returned with error: \(error)")
                    deferred.failure(error: error)
                }
                self.refreshController.endRefreshing()
                FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
            }
        }
        return deferred
        
    }
    
    // MARK: - SPRTableViewController
    
    public override func loadObjectDataSource() -> FutureResult<ObjectDataSource<Any>> {
        if self.isSearching {
            let deferred = DeferredResult<ObjectDataSource<Any>>()
            if self.searchController.searchBar.text?.count == 0 {
                let dataSource = ArrayObjectDataSource(objects: self.dataSource)
                deferred.success(value: dataSource.asAny())
            } else {
                let dataSource = ArrayObjectDataSource(objects: self.searchDataSource)
                deferred.success(value: dataSource.asAny())
            }
            return deferred
        } else {
            if self.dataSource.count > 0 {
                let deferred = DeferredResult<ObjectDataSource<Any>>()
                let dataSource = ArrayObjectDataSource(objects: self.dataSource)
                deferred.success(value: dataSource.asAny())
                return deferred
            } else {
                return self.readMAWBs()
            }
        }
    }
    
    public override func renderCell(in tableView: UITableView, withModel model: Any, at indexPath: IndexPath) throws -> UITableViewCell {
        
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "MAWBCell", for: indexPath)
        
        guard
            let cell = safeCell as? MAWBCell,
            let screenModel = model as? MAWBScreenModel
        else { return safeCell }
        
        cell.dataModel = screenModel
        self.populate(cell: cell, with: screenModel)
        
        return cell
    }
    
    public override func renderCell(in tableView: UITableView, withError error: Error, at indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    // MARK: - Helper Functions
    
    private func configureSearchBarLayout(searchBar: UISearchBar) {
        searchBar.tintColor = .white
        searchBar.placeholder = "Search in this inbound"
        searchBar.scopeButtonTitles = ["By MAWB", "By House No.", "By Rack", "By Pallet No."]
        searchBar.delegate = self
        
        // Only change cursor color without changing Tint Color
        searchBar.subviews[0].subviews.flatMap(){ $0 as? UITextField }.first?.tintColor = UIColor.ICM.black
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.white
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor = UIColor.white
                
                // Rounded corner
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
    }
    
    private func initializeSearchController() {
        self.searchController = UISearchController(searchResultsController: nil)
        self.configureSearchBarLayout(searchBar: self.searchController.searchBar)
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.navigationItem.searchController = self.searchController
        self.definesPresentationContext = true
        self.navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func initRefreshControl() {
        self.tableView.addSubview(self.refreshController)
        
        // Configure
        self.refreshController.addTarget(self, action: #selector(self.refreshDataSource), for: .valueChanged)
    }
    
    private func layoutViews() {
        self.tableView.addSubview(self.noEntryContainer)
        self.noEntryContainer.translatesAutoresizingMaskIntoConstraints = false
        self.noEntryContainer.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor).isActive = true
        self.noEntryContainer.centerYAnchor.constraint(equalTo: self.tableView.centerYAnchor).isActive = true
        self.tableView.tableFooterView = UIView()
    }
    
    private func populate(cell: MAWBCell, with screenModel: MAWBScreenModel) {
        cell.submittedTimeLabel.text = screenModel.submittedTimeValue
        cell.MAWBLabel.text = screenModel.MAWBValue
        cell.rackNumberLabel.text = screenModel.rackNumberValue
        cell.palletNumberLabel.text = screenModel.palletNumberValue
        cell.freightCountLabel.text = screenModel.freightCountValue
    }
    
    @objc private func refreshDataSource() {
        self.dataSource = [MAWBScreenModel]()
        _ = self.readMAWBs(delay: 1.0)
    }
    
    private func showRequestFailedAlert() {
        let alertController = UIAlertController(
            title: "Submission Failed",
            message: "Error occurred while submitting entry. Please try again.",
            preferredStyle: .alert
        )
        
        let okayAction = UIAlertAction(
            title: "Okay",
            style: .default,
            handler: nil
        )
        
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func updateSearchStatus(with searchController: UISearchController) {
        if searchController.isActive {
            self.isSearching = true
        } else {
            self.isSearching = false
        }
    }
    
    // MARK: - UIScrollView Functions
    
    public override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchController.searchBar.resignFirstResponder()
    }
    
    // MARK: - UITableViewDelegate Functions
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard
            let cell = tableView.cellForRow(at: indexPath) as? MAWBCell,
            let screenModel = cell.dataModel
        else { return }
        
        self.performSegue(withIdentifier: "showFullDetailVC", sender: screenModel)
    }
    
    // MARK: - UISearchBarDelegate Functions
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.searchScope = selectedScope
        self.updateSearchResults(for: self.searchController)
    }
    
    // MARK: - UISearchResultsUpdating Functions
    
    public func updateSearchResults(for searchController: UISearchController) {
        self.updateSearchStatus(with: searchController)
        
        if self.isSearching {
            if let searchText = searchController.searchBar.text {
                self.filterContentForSearchText(searchText)
            }
        } else {
            self.setNeedsModelLoaded()
        }
    }
    
    public func filterContentForSearchText(_ searchText: String) {
        let filtered = self.dataSource.filter { (model) -> Bool in
            switch self.searchScope {
            case 0: // by MAWB
                return model.MAWBValue == searchText
            case 1: // by House No.
                return false
            case 2: // by Rack
                return model.rackNumberValue == searchText
            case 3: // by Pallet No.
                return model.palletNumberValue == searchText
            default:
                return false
            }
        }
        self.searchDataSource = filtered
        self.setNeedsModelLoaded()
    }
    
    // MARK: - Navigation Functions
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == "showFullDetailVC" {
            guard
                let destination = segue.destination as? FullDetailVC,
                let screenModel = sender as? MAWBScreenModel
            else { return }
            
            destination.MAWB = screenModel.MAWBValue
            destination.hidesBottomBarWhenPushed = true
        }
    }
}
