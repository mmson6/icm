//
//  EntrySelectionVC.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class EntrySelectionVC: UIViewController {

    @IBOutlet weak var MAWBButton: UIButton!
    @IBOutlet weak var paletteButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    
    public var inboundID: String?
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
    }

    // MARK: - Helper Functions
    
    private func layoutViews() {
        self.bothButton.contentHorizontalAlignment = .center
        
        self.MAWBButton.layer.cornerRadius = 12
        self.paletteButton.layer.cornerRadius = 12
        self.bothButton.layer.cornerRadius = 12
    }
    
    // MARK: - Navigation

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == "showEntryCoordinator" {
            guard
                let button = sender as? UIButton,
                let destination = segue.destination as? EntryCoordinatorVC
            else { return }
            
            destination.inboundID = self.inboundID
            destination.entryOption = button.tag
        }
    }
    
    @IBAction func unwindToEntrySelectionVC(segue: UIStoryboardSegue) {}
}
