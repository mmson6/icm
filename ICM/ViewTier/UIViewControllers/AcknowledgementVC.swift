//
//  AcknowledgementVC.swift
//  ICM
//
//  Created by Mikael Son on 2/21/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class AcknowledgementVC: UIViewController {
    
    @IBOutlet private weak var webView: UIWebView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Open Source Libraries"
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAcknowledgement()
    }
    
    private func showAcknowledgement() {
        let htmlFile = Bundle.main.path(forResource: "Acknowledgements", ofType: "html")
        let html = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
        self.webView.loadHTMLString(html!, baseURL: nil)
    }
}
