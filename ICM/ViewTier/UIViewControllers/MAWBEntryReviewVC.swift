//
//  MAWBEntryReviewVC.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol MAWBEntryReviewVCDelegate: class {
    func backgroundTappedToDismiss()
    func MAWBEntrySubmitted()
    func submitButtonTapped(with dataModel: MAWBEntryModel)
}

public class MAWBEntryReviewVC: UIViewController {

    weak var delegate: MAWBEntryReviewVCDelegate?
    
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var MAWBLabel: UILabel!
    @IBOutlet private weak var freightCountLabel: UILabel!
    @IBOutlet private weak var palletNumbLabel: UILabel!
    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var rackAssignmentLabel: UILabel!
    
    public var dataModel: MAWBEntryModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.loadData()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        
        self.cancelButton.layer.cornerRadius = self.cancelButton.frame.height / 2
        self.cancelButton.layer.borderColor = self.cancelButton.currentTitleColor.cgColor
        self.cancelButton.layer.borderWidth = 1
        
        self.submitButton.tintColor = .white
        self.submitButton.layer.cornerRadius = self.submitButton.frame.height / 2
        self.submitButton.backgroundColor = UIColor.ICM.orange
    }
    
    private func loadData() {
        guard let model = self.dataModel else { return }
        
        self.timeLabel.text = model.timeValue
        self.palletNumbLabel.text = model.palletNumbValue
        self.MAWBLabel.text = model.MAWBValue
        self.freightCountLabel.text = "\(model.freightCountStringValue)"
        self.rackAssignmentLabel.text = model.rackValue
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        if let dataModel = self.dataModel {
            self.delegate?.submitButtonTapped(with: dataModel)
        }
    }
}
