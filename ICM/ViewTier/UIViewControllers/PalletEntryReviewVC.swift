//
//  PalletEntryReviewVC.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol PalletEntryReviewVCDelegate: class {
    func backgroundTappedToDismiss()
    func palletEntrySubmitted()
}

public class PalletEntryReviewVC: UIViewController {

    weak var delegate: PalletEntryReviewVCDelegate?
    
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var palletNumbLabel: UILabel!
    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    
    var dataModel: PalletEntryModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.loadData()
    }
    
    // MARK: - Commands
    
    private func submitEntry() {
        guard let model = self.dataModel else { return }
        let cmd = SubmitPalletEntryCommand()
        cmd.execute(dataModel: model).then { (result) in
            switch result {
            case .success(let success):
                if success {
                    NSLog("Pallet Entry Submitted Successfully")
                    self.delegate?.palletEntrySubmitted()
                } else {
                    NSLog("Pallet Entry Submission failed")
                }
            case .failure(let error):
                NSLog("Failed to submit Pallet Entry : \(error)")
            }
            self.delegate?.backgroundTappedToDismiss()
        }
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        
        self.cancelButton.layer.cornerRadius = self.cancelButton.frame.height / 2
        self.cancelButton.layer.borderColor = self.cancelButton.currentTitleColor.cgColor
        self.cancelButton.layer.borderWidth = 1
        
        self.submitButton.tintColor = .white
        self.submitButton.layer.cornerRadius = self.submitButton.frame.height / 2
        self.submitButton.backgroundColor = UIColor.ICM.orange
    }
    
    private func loadData() {
        guard let model = self.dataModel else { return }
        
        self.palletNumbLabel.text = model.palletNumbValue
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        self.submitEntry()
    }
}
