//
//  SetDateAndTimeVC.swift
//  ICM
//
//  Created by Mikael Son on 2/9/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol SetDateAndTimeVCDelegate: class {
    func backgroundTappedToDismiss()
}

public class SetDateAndTimeVC: UIViewController {

    weak var delegate: SetDateAndTimeVCDelegate?
    
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var currentTimeButton: UIButton!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        self.saveButton.layer.cornerRadius = self.saveButton.frame.height / 2
        self.saveButton.tintColor = .white
        self.saveButton.backgroundColor = UIColor.ICM.blue
        self.currentTimeButton.layer.cornerRadius = self.saveButton.frame.height / 2
        self.currentTimeButton.layer.borderWidth = 1
        self.currentTimeButton.layer.borderColor = UIColor.ICM.gray.cgColor
        self.currentTimeButton.tintColor = UIColor.ICM.gray
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
}
