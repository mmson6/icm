//
//  AddInboudnTicketVC.swift
//  ICM
//
//  Created by Mikael Son on 2/11/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit


public protocol AddInboudnTicketVCDelegate: class {
    func backgroundTappedToDismiss()
    func submitButtonTapped(with dataModel: InboundEntryModel)
}

public class AddInboudnTicketVC: UIViewController, SetDateAndTimeVCDelegate, InboundEntryReviewVCDelegate {

    weak var delegate: AddInboudnTicketVCDelegate?
    
    @IBOutlet private weak var weightTextField: UITextField!
    @IBOutlet private weak var palletCountTextField: UITextField!
    @IBOutlet private weak var createTimeLabel: UILabel!
    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var fromICNButton: UIButton!
    @IBOutlet private weak var fromORDButton: UIButton!
    @IBOutlet private weak var toICNButton: UIButton!
    @IBOutlet private weak var toORDButton: UIButton!
    
    public var year: String?
    private var from: String = ""
    private var to: String = ""
    private var presentingWindowRef: UIWindow?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.initializeCreateTime()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        self.fromICNButton.layer.borderWidth = 1
        self.fromICNButton.layer.cornerRadius = 5
        self.fromICNButton.layer.borderColor = UIColor.ICM.blue.cgColor
        
        self.fromORDButton.layer.borderWidth = 1
        self.fromORDButton.layer.cornerRadius = 5
        self.fromORDButton.layer.borderColor = UIColor.ICM.blue.cgColor
        
        self.toICNButton.layer.borderWidth = 1
        self.toICNButton.layer.cornerRadius = 5
        self.toICNButton.layer.borderColor = UIColor.ICM.blue.cgColor
        
        self.toORDButton.layer.borderWidth = 1
        self.toORDButton.layer.cornerRadius = 5
        self.toORDButton.layer.borderColor = UIColor.ICM.blue.cgColor
        
        self.addButton.backgroundColor = UIColor.ICM.orange
        self.addButton.setTitleColor(.white, for: .normal)
        self.addButton.layer.cornerRadius = self.addButton.frame.height / 2
        
        self.disableAddButton()
    }
    
    private func initializeCreateTime() {
        self.createTimeLabel.text = Date.getCurrentTime().toString()
    }
    
    private func disableAddButton() {
        self.addButton.alpha = 0.5
        self.addButton.isUserInteractionEnabled = false
    }
    
    private func enableAddButton() {
        self.addButton.alpha = 1
        self.addButton.isUserInteractionEnabled = true
    }
    
    private func validate() -> Bool {
        return true
    }
    
    private func dismissOverlayWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingWindowRef?.alpha = 0
        }) { (success) in
            self.presentingWindowRef = nil
        }
    }
    
    private func updateSubmitButton() {
        if self.from != "" && self.to != "" &&
            self.palletCountTextField.text != "" && self.weightTextField.text != "" {
            self.enableAddButton()
        } else {
            self.disableAddButton()
        }
    }
    
    private func showInboundEntryConfirmationView(for dataModel: InboundEntryModel) {
        let storyboard = UIStoryboard(name: "InboundEntrySelection", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "InboundEntryReviewVC") as? InboundEntryReviewVC
        else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        vc.dataModel = dataModel
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3) {
            window.alpha = 1
        }
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func addButtonTapped(_ sender: Any) {
        if self.validate() {
            guard
                let year = self.year,
                let palletCount = self.palletCountTextField.text,
                let weight = self.weightTextField.text,
                let time = self.createTimeLabel.text
            else { return }
            
            let id = CommonCrypto.md5(time)
            
            let dataModel = InboundEntryModel(
                id: id,
                year: year,
                palletCount: palletCount,
                weight: weight,
                from: self.from,
                to: self.to,
                time: time)
            
            self.showInboundEntryConfirmationView(for: dataModel)
        }
    }
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
    
    @IBAction func fromICNTapped(_ sender: UIButton) {
        sender.backgroundColor = UIColor.ICM.blue
        sender.setTitleColor(.white, for: .normal)
        
        self.fromORDButton.backgroundColor = .white
        self.fromORDButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        self.toORDButton.backgroundColor = UIColor.ICM.blue
        self.toORDButton.setTitleColor(.white, for: .normal)
        self.toICNButton.backgroundColor = .white
        self.toICNButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        
        self.from = "ICN"
        self.to = "ORD"
        self.updateSubmitButton()
    }
    
    @IBAction func fromORDTapped(_ sender: UIButton) {
        sender.backgroundColor = UIColor.ICM.blue
        sender.setTitleColor(.white, for: .normal)
        
        self.fromICNButton.backgroundColor = .white
        self.fromICNButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        self.toICNButton.backgroundColor = UIColor.ICM.blue
        self.toICNButton.setTitleColor(.white, for: .normal)
        self.toORDButton.backgroundColor = .white
        self.toORDButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        
        self.from = "ORD"
        self.to = "ICN"
        self.updateSubmitButton()
    }
    
    @IBAction func toICNTapped(_ sender: UIButton) {
        sender.backgroundColor = UIColor.ICM.blue
        sender.setTitleColor(.white, for: .normal)
        
        self.toORDButton.backgroundColor = .white
        self.toORDButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        self.fromORDButton.backgroundColor = UIColor.ICM.blue
        self.fromORDButton.setTitleColor(.white, for: .normal)
        self.fromICNButton.backgroundColor = .white
        self.fromICNButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        
        self.from = "ORD"
        self.to = "ICN"
        self.updateSubmitButton()
    }
    
    @IBAction func toORDTapped(_ sender: UIButton) {
        sender.backgroundColor = UIColor.ICM.blue
        sender.setTitleColor(.white, for: .normal)
        
        self.toICNButton.backgroundColor = .white
        self.toICNButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        self.fromICNButton.backgroundColor = UIColor.ICM.blue
        self.fromICNButton.setTitleColor(.white, for: .normal)
        self.fromORDButton.backgroundColor = .white
        self.fromORDButton.setTitleColor(UIColor.ICM.blue, for: .normal)
        
        self.from = "ICN"
        self.to = "ORD"
        self.updateSubmitButton()
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        // MIKE TODO:: Update so that weight and count only takes number
        self.updateSubmitButton()
    }
    
    // MARK: - InboundEntryReviewVCDelegate Functions
    
    public func backgroundTappedToDismiss() {
        self.dismissOverlayWindow()
    }
    
    public func submitButtonTapped(with dataModel: InboundEntryModel) {
        self.delegate?.submitButtonTapped(with: dataModel)
    }
}
