//
//  MonitorVC.swift
//  ICM
//
//  Created by Mikael Son on 2/19/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

import UIKit

public class MonitorVC: UIViewController, UITableViewDelegate, UITableViewDataSource, InboundYearSelectionVCDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet private weak var inboundSearchButton: UIButton!
    @IBOutlet private weak var yearButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private var noInboundsContainer: UIView!
    
    private let refreshControl = UIRefreshControl()
    private var presentingWindowRef: UIWindow?
    private var selectedYear: String?
    private var searchController = UISearchController()
    private var searchDataSource: Any?
    private var dataSource = [InboundScreenModel]()
    private var sectionDataSource = [InboundSectionScreenModel]()
    private var searchTimer = Timer()
    private var searchScope: Int = 0
    private var isSearching: Bool = false
    private var searchInbounds: Bool = false
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.initRefreshControl()
        self.initializeSearchController()
        self.loadDataSource()
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.searchController.isActive = false
    }
    
    // MARK: - Commands
    
    private func readInbounds(delay: Double = 0) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            let cmd = ReadInboundsCommand()
            cmd.execute(for: self.selectedYear).then { (result) in
                switch result {
                case .success(let screenModels):
                    NSLog("ReadInboundsCommand returned with success")
                    if screenModels.count == 0 {
                        self.noInboundsContainer.isHidden = false
                        self.tableView.separatorStyle = .none
                    } else {
                        self.noInboundsContainer.isHidden = true
                        self.tableView.separatorStyle = .singleLine
                    }
                    self.dataSource = screenModels
                    self.sortDataSourceForSection()
                    self.tableView.reloadData()
                case .failure(let error):
                    NSLog("ReadInboundsCommand returned with error: \(error)")
                }
                self.refreshControl.endRefreshing()
                FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
            }
        }
    }
    
    private func readInboundsByMAWB() {
        let searchText = self.searchController.searchBar.text
        
        let cmd = ReadInboundsByMAWBCommand()
        cmd.execute(with: searchText).then { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("ReadInboundsByMAWBCommand returned with success")
                self.loadDataSource(with: screenModels)
            case .failure(let error):
                NSLog("ReadInboundsByMAWBCommand returned with error: \(error)")
            }
        }
    }
    
    private func readInboundsByPallet() {
        let searchText = self.searchController.searchBar.text
        
        let cmd = ReadInboundsByPalletCommand()
        cmd.execute(with: searchText).then { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("ReadInboundsByPalletCommand returned with success")
                self.loadDataSource(with: screenModels)
            case .failure(let error):
                NSLog("ReadInboundsByPalletCommand returned with error: \(error)")
            }
        }
    }
    
    private func readInboundsByRack() {
        let searchText = self.searchController.searchBar.text
        
        let cmd = ReadInboundsByRackCommand()
        cmd.execute(with: searchText).then { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("ReadInboundsByRackCommand returned with success")
                self.loadDataSource(with: screenModels)
            case .failure(let error):
                NSLog("ReadInboundsByRackCommand returned with error: \(error)")
            }
        }
    }
    
    private func readMAWBWithMAWB() {
        let searchText = self.searchController.searchBar.text
        
        let cmd = ReadMAWBWithMAWBCommand()
        cmd.execute(with: searchText?.lowercased()).then(handleResult: { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("readMAWB returned with success")
                self.loadDataSource(with: screenModels)
            case .failure(let error):
                NSLog("readMAWB returned with error: \(error)")
            }
        })
    }
    
    private func readMAWBsByPallet() {
        let searchText = self.searchController.searchBar.text
        
        let cmd = ReadMAWBsByPalletCommand()
        cmd.execute(with: searchText?.lowercased()).then(handleResult: { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("ReadMAWBsByPalletCommand returned with success")
                self.loadDataSource(with: screenModels)
            case .failure(let error):
                NSLog("ReadMAWBsByPalletCommand returned with error: \(error)")
            }
        })
    }
    
    private func readMAWBsByRack() {
        let searchText = self.searchController.searchBar.text
        
        let cmd = ReadMAWBsByRackCommand()
        cmd.execute(with: searchText?.lowercased()).then(handleResult: { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("ReadMAWBsByRackCommand returned with success")
                self.loadDataSource(with: screenModels)
            case .failure(let error):
                NSLog("ReadMAWBsByRackCommand returned with error: \(error)")
            }
        })
    }
    
    // MARK: - Helper Functions
    
    private func configureSearchBarLayout(searchBar: UISearchBar) {
        searchBar.tintColor = .white
        searchBar.placeholder = "Globally search MAWBs"
        searchBar.scopeButtonTitles = ["By MAWB", "By House No.", "By Rack", "By Pallet No."]
        searchBar.delegate = self
        
        // Only change cursor color without changing Tint Color
        searchBar.subviews[0].subviews.flatMap(){ $0 as? UITextField }.first?.tintColor = UIColor.ICM.black
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.white
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor = UIColor.white
                
                // Rounded corner
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
    }
    
    private func dismissOverlayWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingWindowRef?.alpha = 0
        }) { (success) in
            self.presentingWindowRef = nil
        }
    }
    
    private func initRefreshControl() {
        self.tableView.addSubview(self.refreshControl)
        
        // Configure
        self.refreshControl.addTarget(self, action: #selector(self.refreshDataSource), for: .valueChanged)
        self.refreshControl.isUserInteractionEnabled = false
    }
    
    private func layoutViews() {
        self.setCurrentYear()
        
        self.inboundSearchButton.layer.borderWidth = 1
        self.inboundSearchButton.layer.borderColor = UIColor.white.cgColor
        self.inboundSearchButton.layer.cornerRadius = 5
        
        self.yearButton.layer.borderWidth = 1
        self.yearButton.layer.borderColor = UIColor.white.cgColor
        self.yearButton.layer.cornerRadius = 5
        
        self.tableView.addSubview(self.noInboundsContainer)
        self.noInboundsContainer.translatesAutoresizingMaskIntoConstraints = false
        self.noInboundsContainer.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor).isActive = true
        self.noInboundsContainer.centerYAnchor.constraint(equalTo: self.tableView.centerYAnchor).isActive = true
        self.tableView.tableFooterView = UIView()
    }
    
    private func initializeSearchController() {
        self.searchController = UISearchController(searchResultsController: nil)
        self.configureSearchBarLayout(searchBar: self.searchController.searchBar)
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.navigationItem.searchController = self.searchController
        self.definesPresentationContext = true
        self.navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func loadDataSource(with searchData: Any? = nil) {
        if self.isSearching {
            self.searchDataSource = searchData
            self.tableView.reloadData()
        } else {
            if self.dataSource.count > 0 {
                if self.sectionDataSource.count == 0 {
                    self.sortDataSourceForSection()
                }
                self.tableView.reloadData()
            } else {
                self.readInbounds()
            }
        }
    }
    
    @objc private func readSearchData() {
        switch self.searchScope {
        case 0: // MAWB
            self.searchInbounds ? self.readInboundsByMAWB() : self.readMAWBWithMAWB()
        case 1: // House
            return
        case 2: // Rack
            // MIKE TODO: needs testing after index set up
            self.searchInbounds ? self.readInboundsByRack() : self.readMAWBsByRack()
        case 3: // Pallet
            // MIKE TODO: needs testing after index set up
            self.searchInbounds ? self.readInboundsByPallet() : self.readMAWBsByPallet()
        default:
            return
        }
    }
    
    @objc private func refreshDataSource() {
        self.dataSource = [InboundScreenModel]()
        self.readInbounds(delay: 1.0)
    }
    
    private func setCurrentYear() {
        let date = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: date)
        self.selectedYear = "\(currentYear)"
        self.yearButton.setTitle("\(currentYear)", for: .normal)
    }
    
    private func setSearchTimer() {
        self.searchTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.readSearchData), userInfo: nil, repeats: false)
    }
    
    private func sortDataSourceForSection() {
        var dataSource = [InboundSectionScreenModel]()
        
        var tempSectionModel = InboundSectionScreenModel(month: "")
        var tempDataSource = [InboundScreenModel]()
        for object in self.dataSource {
            let date = object.timeValue.toDate()
            let calendar = Calendar.current
            let month = calendar.component(.month, from: date).toStringMonth()
            if month != tempSectionModel.month {
                if tempDataSource.count > 0 {
                    tempSectionModel.dataSource = tempDataSource
                    dataSource.append(tempSectionModel)
                }
                let sectionModel = InboundSectionScreenModel(month: month)
                tempSectionModel = sectionModel
                tempDataSource = [InboundScreenModel]()
                tempDataSource.append(object)
            } else {
                tempDataSource.append(object)
            }
        }
        
        if tempDataSource.count > 0 {
            tempSectionModel.dataSource = tempDataSource
            dataSource.append(tempSectionModel)
        }
        self.sectionDataSource = dataSource
    }
    
    private func updateSearchStatus(with searchController: UISearchController) {
        if searchController.isActive && searchController.searchBar.text != "" {
            self.isSearching = true
        } else {
            self.isSearching = false
        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchController.searchBar.resignFirstResponder()
    }
    
    // MARK: - UITableViewCell Related Functions
    
    private func getInboundCell(with indexPath: IndexPath) -> UITableViewCell {
        let safeCell = self.tableView.dequeueReusableCell(withIdentifier: "InboundCell", for: indexPath)
        
        let sectionModel = self.sectionDataSource[indexPath.section]
        let screenModel = sectionModel.dataSource[indexPath.row]
        guard
            let cell = safeCell as? InboundCell
        else { return safeCell }
        
        cell.dataModel = screenModel
        self.populate(cell: cell, with: screenModel)
        
        return cell
    }
    
    private func getInboundSearchCell(with indexPath: IndexPath) -> UITableViewCell {
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "InboundCell", for: indexPath)
        
        guard
            let cell = safeCell as? InboundCell,
            let dataSource = self.searchDataSource as? [InboundScreenModel]
        else { return safeCell }
        let screenModel = dataSource[indexPath.row]
        
        cell.dataModel = screenModel
        self.populate(cell: cell, with: screenModel)
        
        return cell
    }
    
    private func getMAWBCell(with indexPath: IndexPath) -> UITableViewCell {
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "MAWBCell", for: indexPath)
        
        guard
            let cell = safeCell as? MAWBCell,
            let dataSource = self.searchDataSource as? [MAWBScreenModel]
        else { return safeCell }
        let screenModel = dataSource[indexPath.row]
        
        cell.dataModel = screenModel
        self.populate(cell: cell, with: screenModel)
        
        return cell
    }
    
    private func populate(cell: InboundCell, with screenModel: InboundScreenModel) {
        var timeString = ""
        if screenModel.fromValue == "ICN" {
            timeString = String(format: "\(screenModel.timeValue), South Korea")
        } else {
            timeString = screenModel.timeValue
        }
        cell.timeLabel.text = timeString
        cell.palletCountLabel.text = screenModel.palletCountValue
        cell.fromLabel.text = screenModel.fromValue
        cell.toLabel.text = screenModel.toValue
        cell.weightLabel.text = screenModel.weightValue
    }
    
    private func populate(cell: MAWBCell, with screenModel: MAWBScreenModel) {
        cell.submittedTimeLabel.text = screenModel.submittedTimeValue
        cell.MAWBLabel.text = screenModel.MAWBValue
        cell.rackNumberLabel.text = screenModel.rackNumberValue
        cell.palletNumberLabel.text = screenModel.palletNumberValue
        cell.freightCountLabel.text = screenModel.freightCountValue
    }
    
    // MARK: - UITableViewDelegate Functions
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.isSearching {
            let dataSource = self.searchDataSource as? [Any]
            if dataSource?.count == 0 {
                return 0
            }
        } else {
            if self.dataSource.count == 0 {
                return 0
            }
        }
        
        return self.isSearching && !self.searchInbounds ? 0 : 28
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let month = self.sectionDataSource[section].month
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 90))
        view.backgroundColor = UIColor.ICM.lightGray
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width - 100, height: 50))
        label.text = month
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.ICM.black
        view.addSubview(label)

        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 25).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        return view
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if self.isSearching {
            return 1
        } else {
            return self.sectionDataSource.count
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching {
            if let dataSource = self.searchDataSource as? [Any] {
                return dataSource.count
            }
            return 0
        } else {
            let sectionModel = self.sectionDataSource[section]
            return sectionModel.dataSource.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isSearching {
            switch self.searchScope {
            case 0:
                return self.searchInbounds ?
                    self.getInboundSearchCell(with: indexPath) : self.getMAWBCell(with: indexPath)
            case 1:
                break
            case 2:
                return self.searchInbounds ?
                    self.getInboundSearchCell(with: indexPath) : self.getMAWBCell(with: indexPath)
            case 3:
                return self.searchInbounds ?
                    self.getInboundSearchCell(with: indexPath) : self.getMAWBCell(with: indexPath)
            default:
                break
            }
        } else {
            return self.getInboundCell(with: indexPath)
        }
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.isSearching && !self.searchInbounds {
            guard
                let cell = tableView.cellForRow(at: indexPath) as? MAWBCell,
                let screenModel = cell.dataModel
            else { return }
            
            self.performSegue(withIdentifier: "showFullDetailVC", sender: screenModel)
        } else {
            guard
                let cell = tableView.cellForRow(at: indexPath) as? InboundCell,
                let screenModel = cell.dataModel
            else { return }
            
            self.performSegue(withIdentifier: "showMAWBListVC", sender: screenModel)
        }
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func inboundSearchOptionButtonTapped(_ sender: UIButton) {
        self.searchInbounds = !self.searchInbounds
        self.updateSearchResults(for: self.searchController)
        if self.searchInbounds {
            self.searchController.searchBar.placeholder = "Globally search inbounds"
            self.inboundSearchButton.backgroundColor = .white
            self.inboundSearchButton.setTitleColor(UIColor.ICM.gray, for: .normal)
        } else {
            self.searchController.searchBar.placeholder = "Globally search MAWBs"
            self.inboundSearchButton.backgroundColor = UIColor.ICM.gray
            self.inboundSearchButton.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBAction func yearButtonTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "InboundEntrySelection", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "InboundYearSelectionVC") as? InboundYearSelectionVC
        else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3, animations: {
            window.alpha = 1
        })
    }
    
    // MARK: - InboundYearSelectionVCDelegate Functions
    
    public func backgroundTappedToDismiss() {
        self.dismissOverlayWindow()
    }
    
    public func yearSelected(with year: String) {
        self.selectedYear = year
        self.yearButton.setTitle(year, for: .normal)
        self.dataSource = [InboundScreenModel]()
        self.loadDataSource()
        self.dismissOverlayWindow()
    }
    
    // MARK: - UISearchBarDelegate Functions
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.searchScope = selectedScope
        self.updateSearchResults(for: self.searchController)
    }
    
    // MARK: - UISearchResultsUpdating Functions
    
    public func updateSearchResults(for searchController: UISearchController) {
        self.updateSearchStatus(with: searchController)
        
        if self.isSearching {
            self.refreshControl.removeFromSuperview()
        } else {
            self.tableView.addSubview(self.refreshControl)
        }

        
        if self.searchTimer.isValid {
            self.searchTimer.invalidate()
        }
        
        if self.isSearching {
            self.setSearchTimer()
        } else {
            self.loadDataSource()
        }
    }
    
    public func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        
    }
    
    // MARK: - Navigation Functions
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == "showMAWBListVC" {
            guard
                let destination = segue.destination as? MAWBListVC,
                let screenModel = sender as? InboundScreenModel
            else { return }
            
            destination.inboundID = screenModel.IDValue
            
        } else if identifier == "showFullDetailVC" {
            guard
                let destination = segue.destination as? FullDetailVC,
                let screenModel = sender as? MAWBScreenModel
            else { return }
            
            destination.MAWB = screenModel.MAWBValue
            destination.hidesBottomBarWhenPushed = true
        }
    }
}

