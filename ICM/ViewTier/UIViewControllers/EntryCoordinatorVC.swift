//
//  EntryCoordinatorVC.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class EntryCoordinatorVC: UIViewController, PalletEntryVCDelegate, MAWBEntryVCDelegate {

    // Action Buttions
    @IBOutlet private weak var MAWBToggleButton: UIButton!
    @IBOutlet private weak var palletToggleButton: UIButton!
    
    // When nothing to display
    @IBOutlet weak var closeThisScreenButton: UIButton!
    @IBOutlet weak var noViewsContainer: UIView!
    
    // Container Views
    @IBOutlet weak var containerViewOne: UIView!
    @IBOutlet weak var containerViewTwo: UIView!
    
    // Constraints
    @IBOutlet weak var navBarWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewTrailingToMargin: NSLayoutConstraint!
    @IBOutlet weak var stackViewTrailingToNav: NSLayoutConstraint!
    @IBOutlet weak var stackViewTopToMargin: NSLayoutConstraint!
    @IBOutlet weak var stackViewTopToNav: NSLayoutConstraint!
    
    // Views
    @IBOutlet weak var actionButtonStackView: UIStackView!
    @IBOutlet weak var stackView: UIStackView!
    
    // ContainerView childController and subview reference
    private var subViewRefOne: UIView?
    private var subViewRefTwo: UIView?
    private var childControllerRefOne: UIViewController?
    private var childControllerRefTwo: UIViewController?
    
    public var inboundID: String?
    public var entryOption: Int = 0
    
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIDevice.current.orientation.isLandscape {
            self.stackView.axis = .horizontal
        } else {
            self.stackView.axis = .vertical
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.handleLoadContainerViews()
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
        
        self.MAWBToggleButton.layer.borderWidth = 1
        self.MAWBToggleButton.layer.borderColor = UIColor.white.cgColor
        self.MAWBToggleButton.layer.cornerRadius = 5
        self.palletToggleButton.layer.borderWidth = 1
        self.palletToggleButton.layer.borderColor = UIColor.white.cgColor
        self.palletToggleButton.layer.cornerRadius = 5
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.noViewsContainer.isHidden = true
        self.closeThisScreenButton.layer.borderWidth = 1
        self.closeThisScreenButton.layer.borderColor = UIColor.ICM.blue.cgColor
        self.closeThisScreenButton.layer.cornerRadius = 5
    }
    
    private func handleLoadContainerViews() {
        
        switch self.entryOption {
        case 1:
            self.containerViewTwo.isHidden = true
            self.fetchContainerViewOne(fromLoad: true)
        case 2:
            self.containerViewOne.isHidden = true
            self.fetchContainerViewTwo(fromLoad: true)
        case 3:
            self.fetchContainerViewOne()
            self.fetchContainerViewTwo()
        default:
            break
        }
        
        self.updateActionButtonStatus()
    }
    
    private func fetchContainerViewOne(fromLoad: Bool = false, withToast: Bool = false) {
        let storyboard = UIStoryboard.init(name: "Entry", bundle: nil)
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "MAWBEntryVC") as? MAWBEntryVC
        else { return }
        
        vc.delegate = self
        self.subViewRefOne = vc.view
        self.childControllerRefOne = vc
        
        vc.willMove(toParentViewController: self)
        self.containerViewOne.addSubview(vc.view)
        vc.view.frame.size = self.containerViewOne.frame.size
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
        
        if fromLoad {
            vc.palletNumbTextField.becomeFirstResponder()
        }
        if withToast {
            vc.showDropDownToast(message: "Submitted Successfully", duration: 2.0)
        }
    }
    
    private func fetchContainerViewTwo(fromLoad: Bool = false, withToast: Bool = false) {
        let storyboard = UIStoryboard.init(name: "Entry", bundle: nil)
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "PalletEntryVC") as? PalletEntryVC
        else { return }
        
        vc.delegate = self
        self.subViewRefTwo = vc.view
        self.childControllerRefTwo = vc
        
        vc.willMove(toParentViewController: self)
        self.containerViewTwo.addSubview(vc.view)
        vc.view.frame.size = self.containerViewTwo.frame.size
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
        
        if fromLoad {
            vc.palletNumbTextField.becomeFirstResponder()
        }
        if withToast {
            vc.showDropDownToast(message: "Submitted Successfully", duration: 2.0)
        }
    }
    
    private func updateConstraintsForPortrait() {
        self.stackView.axis = .vertical
        self.actionButtonStackView.axis = .horizontal
        self.navBarTopConstraint.constant = 0
        self.navBarLeftConstraint.priority = 999
        self.navBarHeightConstraint.priority = 999
        self.navBarBottomConstraint.priority = 750
        self.navBarWidthConstraint.priority = 750
        
        self.stackViewTopToNav.priority = 999
        self.stackViewTrailingToMargin.priority = 999
        self.stackViewTopToMargin.priority = 750
        self.stackViewTrailingToNav.priority = 750
    }
    
    private func updateConstraintsForLandscape() {
        self.stackView.axis = .horizontal
        self.actionButtonStackView.axis = .vertical
        self.navBarTopConstraint.constant = 20
        self.navBarLeftConstraint.priority = 750
        self.navBarHeightConstraint.priority = 750
        self.navBarBottomConstraint.priority = 999
        self.navBarWidthConstraint.priority = 999
        
        self.stackViewTopToNav.priority = 750
        self.stackViewTrailingToMargin.priority = 750
        self.stackViewTopToMargin.priority = 999
        self.stackViewTrailingToNav.priority = 999
    }
    
    private func updateNoDisplayingViews() {
        self.noViewsContainer.isHidden = !self.containerViewTwo.isHidden || !self.containerViewOne.isHidden
    }
    
    private func updateViewLayouts() {
        self.updateActionButtonStatus()
        self.updateNoDisplayingViews()
    }
    
    private func updateActionButtonStatus() {
        self.updateMAWBButtonLayout()
        self.updatePalletButtonLayout()
    }
    
    private func updateMAWBButtonLayout() {
        if self.containerViewOne.isHidden {
            for childVC in self.childViewControllers {
                if let vc = childVC as? MAWBEntryVC {
                    vc.editingTextFieldRef?.resignFirstResponder()
                }
            }
            self.MAWBToggleButton.backgroundColor = UIColor.ICM.gray
            self.MAWBToggleButton.setTitleColor(.white, for: .normal)
        } else {
            self.MAWBToggleButton.backgroundColor = .white
            self.MAWBToggleButton.setTitleColor(UIColor.ICM.gray, for: .normal)
        }
    }
    
    private func updatePalletButtonLayout() {
        if self.containerViewTwo.isHidden {
            for childVC in self.childViewControllers {
                if let vc = childVC as? PalletEntryVC {
                    vc.editingTextFieldRef?.resignFirstResponder()
                }
            }
            self.palletToggleButton.backgroundColor = UIColor.ICM.gray
            self.palletToggleButton.setTitleColor(.white, for: .normal)
        } else {
            self.palletToggleButton.backgroundColor = .white
            self.palletToggleButton.setTitleColor(UIColor.ICM.gray, for: .normal)
        }
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        for childVC in self.childViewControllers {
            if let vc = childVC as? MAWBEntryVC {
                vc.editingTextFieldRef?.resignFirstResponder()
            }
            if let vc = childVC as? PalletEntryVC {
                vc.editingTextFieldRef?.resignFirstResponder()
            }
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        for childVC in self.childViewControllers {
            if let vc = childVC as? MAWBEntryVC {
                vc.editingTextFieldRef?.resignFirstResponder()
            }
            if let vc = childVC as? PalletEntryVC {
                vc.editingTextFieldRef?.resignFirstResponder()
            }
        }
        self.performSegue(withIdentifier: "unwindToEntrySelectionVC", sender: nil)
    }
    
    @IBAction func toggleBillingNumberTapped(_ sender: Any) {
        
        if self.subViewRefOne == nil {
            self.fetchContainerViewOne()
        }
        
        self.containerViewOne.isHidden = !self.containerViewOne.isHidden
        UIView.animate(withDuration: 0.3) {
            if self.containerViewOne.isHidden {
                self.containerViewOne.alpha = 0
            } else {
                self.containerViewOne.alpha = 1
            }
        }
        self.updateViewLayouts()
    }
    
    @IBAction func togglePalletTapped(_ sender: Any) {
        
        if self.subViewRefTwo == nil {
            self.fetchContainerViewTwo()
        }
        
        self.containerViewTwo.isHidden = !self.containerViewTwo.isHidden
        UIView.animate(withDuration: 0.3) {
            if self.containerViewTwo.isHidden {
                self.containerViewTwo.alpha = 0
            } else {
                self.containerViewTwo.alpha = 1
            }
        }
        self.updateViewLayouts()
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.current.orientation.isLandscape {
            self.stackView.axis = .horizontal
        } else {
            self.stackView.axis = .vertical
        }
    }

    // MARK: - MAWBEntryVCDelegate Functions
    
    public func billNumberEntryCloseButtonTapped(sender: UIViewController) {
        sender.willMove(toParentViewController: nil)
        self.subViewRefOne?.removeFromSuperview()
        self.subViewRefOne = nil
        self.childControllerRefOne?.removeFromParentViewController()
        self.childControllerRefOne = nil
        sender.didMove(toParentViewController: nil)
        self.containerViewOne.isHidden = true
        self.updateViewLayouts()
    }
    
    public func MAWBEntrySubmitted(sender: UIViewController) {
        // Dismiss the VC
        sender.willMove(toParentViewController: nil)
        self.subViewRefOne?.removeFromSuperview()
        self.subViewRefOne = nil
        self.childControllerRefOne?.removeFromParentViewController()
        self.childControllerRefOne = nil
        sender.didMove(toParentViewController: nil)
        
        // Reload empty form
        self.fetchContainerViewOne(withToast: true)
    }
    
    // MARK: - PalletEntryVCDelegate Functions
    
    public func palletEntryCloseButtonTapped(sender: UIViewController) {
        sender.willMove(toParentViewController: nil)
        self.subViewRefTwo?.removeFromSuperview()
        self.subViewRefTwo = nil
        self.childControllerRefTwo?.removeFromParentViewController()
        self.childControllerRefTwo = nil
        sender.didMove(toParentViewController: nil)
        self.containerViewTwo.isHidden = true
        self.updateViewLayouts()
    }
    
    public func palletEntrySubmitted(sender: UIViewController) {
        // Dismiss the VC
        sender.willMove(toParentViewController: nil)
        self.subViewRefTwo?.removeFromSuperview()
        self.subViewRefTwo = nil
        self.childControllerRefTwo?.removeFromParentViewController()
        self.childControllerRefTwo = nil
        sender.didMove(toParentViewController: nil)
        
        // Reload empty form
        self.fetchContainerViewTwo(withToast: true)
    }
}
