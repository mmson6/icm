//
//  ManageRackVC.swift
//  ICM
//
//  Created by Mikael Son on 2/10/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class ManageRacksVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AddRackVCDelegate {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var addSomeRacksButton: UIButton!
    @IBOutlet private weak var noRacksContainer: UIView!
    
    private let refreshControl = UIRefreshControl()
    private var presentingWindowRef: UIWindow?
    private var dataSource = [RackScreenModel]()
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.initRefreshControl()
        self.loadDataSource()
    }
    
    // MARK: - Commands
    
    private func addRack(with dataModel: AddRackModel) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        let cmd = AddRackCommand()
        cmd.execute(dataModel: dataModel).then { (result) in
            switch result {
            case .success(let success):
                if success {
                    NSLog("AddRackCommand returned with success")
                    self.loadDataSource()
                } else {
                    NSLog("AddRackCommand returned with failure")
                }
            case .failure(let error):
                NSLog("AddRackCommand returned with error: \(error)")
            }
            FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
        }
    }
    
    private func deleteRack(with screenModel: RackScreenModel?) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        guard let screenModel = screenModel else { return }
        let dataModel = DeleteRackModel(name: screenModel.nameValue, info: screenModel.infoValue)
        let cmd = DeleteRackCommand()
        cmd.execute(dataModel: dataModel).then { (result) in
            switch result {
            case .success(let success):
                if success {
                    NSLog("DeleteRackCommand returned with success")
                    self.loadDataSource()
                } else {
                    NSLog("DeleteRackCommand returned with failure")
                }
            case .failure(let error):
                NSLog("DeleteRackCommand returned with error: \(error)")
            }
            FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
        }
    }
    
    private func readRacks(delay: Double = 0) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            let cmd = ReadRacksCommand()
            cmd.execute().then { (result) in
                switch result {
                case .success(let screenModels):
                    if screenModels.count == 0 {
                        self.noRacksContainer.isHidden = false
                    } else {
                        self.noRacksContainer.isHidden = true
                    }
                    self.dataSource = screenModels
                    self.tableView.reloadData()
                case .failure(let error):
                    NSLog("ReadRacksCommand returned with error: \(error)")
                }
                self.refreshControl.endRefreshing()
                FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
            }
        }
    }
    
    // MARK: - Helper Functions
    
    private func dismissOverlayWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingWindowRef?.alpha = 0
        }) { (success) in
            self.presentingWindowRef = nil
        }
    }
    
    private func initRefreshControl() {
        self.tableView.refreshControl = self.refreshControl
        
        // Configure
        self.refreshControl.addTarget(self, action: #selector(self.refreshDataSource), for: .valueChanged)
    }
    
    private func layoutViews() {
        self.addSomeRacksButton.layer.borderWidth = 1
        self.addSomeRacksButton.layer.cornerRadius = 5
        self.addSomeRacksButton.layer.borderColor = UIColor.ICM.blue.cgColor
        self.tableView.tableFooterView = UIView()
    }
    
    private func loadDataSource() {
        self.readRacks()
    }
    
    @objc private func refreshDataSource() {
        self.readRacks(delay: 1.0)
    }
    
    private func showDeleteAlertView(for cell: RackCell) {
        let alertController = UIAlertController(
            title: "Delete Rack",
            message: "Do you want to delete this rack?",
            preferredStyle: .alert
        )
        
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil)
        
        let deleteAction = UIAlertAction(
            title: "Delete",
            style: .destructive) { (action) in
                self.deleteRack(with: cell.dataModel)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func showDupliateRackAlert() {
        let alertController = UIAlertController(
            title: "Duplicate Rack",
            message: "You cannot add a rack with same name. Please try other name.",
            preferredStyle: .alert
        )
        
        let okayAction = UIAlertAction(
            title: "Okay",
            style: .default,
            handler: nil
        )
        
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func validate() -> Bool {
        // Mike TODO:: check for duplicate rack name here
        return true
    }
    
    // MARK: - UITableViewDelegate Functions
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "RackCell", for: indexPath)
        
        let screenModel = self.dataSource[indexPath.row]
        guard
            let cell = safeCell as? RackCell
        else { return safeCell }
        
        cell.selectionStyle = .none
        cell.dataModel = screenModel
        cell.nameLabel.text = screenModel.nameValue
        cell.infoLabel.text = screenModel.infoValue
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            guard let cell = tableView.cellForRow(at: indexPath) as? RackCell else { return }
            self.showDeleteAlertView(for: cell)
        }
//        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
//        }
        
        return [delete]
    }
    // MARK: - IBAction Functions
    
    @IBAction func addButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "AddRackVC") as? AddRackVC
        else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3) {
            window.alpha = 1
        }
    }
    
    // MARK: - AddRackVCDelegate
    
    public func addButtonTapped(with dataModel: AddRackModel, sender: 
        AddRackVC) {
        
        self.dismissOverlayWindow()
        if self.validate() {
            self.addRack(with: dataModel)
        } else {
            self.showDupliateRackAlert()
        }
    }
    
    public func backgroundTappedToDismiss() {
        self.dismissOverlayWindow()
    }
}
