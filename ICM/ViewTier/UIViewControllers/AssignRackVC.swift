//
//  AssignRackVC.swift
//  ICM
//
//  Created by Mikael Son on 2/9/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public protocol AssignRackVCDelegate: class {
    func backgroundTappedToDismiss()
    func rackSelectedForAssignment(with screenModel: RackScreenModel)
}

fileprivate let sectionInsets = UIEdgeInsets(top: 40.0, left: 10.0, bottom: 40.0, right: 10.0)
fileprivate let itemsPerRow: CGFloat = 3

public class AssignRackVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    weak var delegate: AssignRackVCDelegate?
    
    private var dataSource = [RackScreenModel]()
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var noRacksContainer: UIView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.loadDataSource()
    }
    
    // MARK: - Commands
    
    private func readRacks() {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        let cmd = ReadRacksCommand()
        cmd.execute().then { (result) in
            switch result {
            case .success(let screenModels):
                if screenModels.count == 0 {
                    self.noRacksContainer.isHidden = false
                } else {
                    self.noRacksContainer.isHidden = true
                }
                self.dataSource = screenModels
                self.collectionView.reloadData()
            case .failure(let error):
                NSLog("ReadRacksCommand returned with error: \(error)")
            }
            FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
        }
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {}
    
    private func loadDataSource() {
        self.readRacks()
    }
    
    // MARK: - UICollectionViewDelegate Functions
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let safeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RackCVCell", for: indexPath)
        
        let screenModel = self.dataSource[indexPath.row]
        guard
            let cell = safeCell as? RackCVCell
        else { return safeCell }
        
        cell.dataModel = screenModel
        cell.nameLabel.text = screenModel.nameValue
        cell.infoLabel.text = screenModel.infoValue
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard
            let cell = collectionView.cellForItem(at: indexPath) as? RackCVCell,
            let screenModel = cell.dataModel
        else { return }
        
        self.delegate?.rackSelectedForAssignment(with: screenModel)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout Functions
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    // MARK: - IBAction Functions
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.delegate?.backgroundTappedToDismiss()
    }
}
