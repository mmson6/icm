//
//  FullDetailVC.swift
//  ICM
//
//  Created by Mikael Son on 2/20/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class FullDetailVC: UIViewController {

    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var MAWBLabel: UILabel!
    @IBOutlet private weak var rackLabel: UILabel!
    @IBOutlet private weak var freightCountLabel: UILabel!
    @IBOutlet private weak var palletNumberLabel: UILabel!
    @IBOutlet private weak var submissionTimeLabel: UILabel!
    @IBOutlet private weak var fromLabel: UILabel!
    @IBOutlet private weak var toLabel: UILabel!
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var palletCountLabel: UILabel!
    @IBOutlet private weak var createdTimeLabel: UILabel!
    
    @IBOutlet private weak var stackViewTopConstraint: NSLayoutConstraint!
    
    public var MAWB: String?
    private var dataSource = [FullDetailScreenModel]()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.readFullDetailMAWB()
    }
    
    // MARK: - Commands
    
    private func readFullDetailMAWB() {
        let cmd = ReadFullDetailCommand()
        cmd.execute(with: self.MAWB).then { (result) in
            switch result {
            case .success(let screenModels):
                NSLog("ReadFullDetailCommand returned with success")
                self.dataSource = screenModels
                self.loadModel()
            case .failure(let error):
                NSLog("ReadFullDetailCommand returned with error: \(error)")
            }
        }
    }
    
    // MARK: - Helper Functions
    
    private func layoutViews() {
    }
    
    private func loadModel() {
        guard let model = self.dataSource.first else { return }

        self.MAWBLabel.text = model.MAWBValue
        self.rackLabel.text = model.rackNumberValue
        self.freightCountLabel.text = model.freightCountValue
        self.palletNumberLabel.text = model.palletNumberValue
        self.submissionTimeLabel.text = model.submittedTimeValue
        self.fromLabel.text = model.fromValue
        self.toLabel.text = model.toValue
        self.weightLabel.text = model.weightValue
        self.palletCountLabel.text = model.palletCountValue
        self.createdTimeLabel.text = model.createdTimeValue
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.current.orientation.isLandscape {
            self.stackView.axis = .horizontal
            self.stackViewTopConstraint.constant = 50
        } else {
            self.stackView.axis = .vertical
            self.stackViewTopConstraint.constant = 0
        }
    }
}
