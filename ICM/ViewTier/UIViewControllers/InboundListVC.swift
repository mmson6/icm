//
//  InboundListVC.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class InboundListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AddInboudnTicketVCDelegate, InboundYearSelectionVCDelegate {
    
    @IBOutlet private weak var yearButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private var noInboundsContainer: UIView!
    
    private let refreshControl = UIRefreshControl()
    private var presentingWindowRef: UIWindow?
    private var selectedYear: String?
    private var dataSource = [InboundScreenModel]()
    private var sectionDataSource = [InboundSectionScreenModel]()
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutViews()
        self.initRefreshControl()
        self.loadDataSource()
    }
    
    // MARK: - Commands
    
    private func addInboundEntry(with dataModel: InboundEntryModel) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        let cmd = SubmitInboundTicketCommand()
        cmd.execute(dataModel: dataModel).then { (result) in
            switch result {
            case .success(let success):
                if success {
                    NSLog("SubmitInboundTicketCommand returned with success")
                    self.loadDataSource()
                } else {
                    NSLog("SubmitInboundTicketCommand returned with failure")
                }
            case .failure(let error):
                NSLog("SubmitInboundTicketCommand returned with error : \(error)")
                self.showRequestFailedAlert()
            }
            FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
        }
    }
    
    private func loadDataSource(delay: Double = 0) {
        FullScreenActivityPresenter.shared.showActivityIndicator(forViewController: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            let cmd = ReadInboundsCommand()
            cmd.execute(for: self.selectedYear).then { (result) in
                switch result {
                case .success(let screenModels):
                    NSLog("ReadInboundsCommand returned with success")
                    if screenModels.count == 0 {
                        self.noInboundsContainer.isHidden = false
                        self.tableView.separatorStyle = .none
                    } else {
                        self.noInboundsContainer.isHidden = true
                        self.tableView.separatorStyle = .singleLine
                    }
                    self.dataSource = screenModels
                    self.sortDataSourceForSection()
                    self.tableView.reloadData()
                case .failure(let error):
                    NSLog("ReadInboundsCommand returned with error: \(error)")
                }
                self.refreshControl.endRefreshing()
                FullScreenActivityPresenter.shared.hideActivityIndicator(forViewController: self)
            }
        }
    }
    
    // MARK: - Helper Functions
    
    private func dismissOverlayWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingWindowRef?.alpha = 0
        }) { (success) in
            self.presentingWindowRef = nil
        }
    }
    
    private func initRefreshControl() {
        self.tableView.refreshControl = self.refreshControl
        
        // Configure
        self.refreshControl.addTarget(self, action: #selector(self.refreshDataSource), for: .valueChanged)
    }
    
    private func layoutViews() {
        self.setCurrentYear()
        
        self.yearButton.layer.borderWidth = 1
        self.yearButton.layer.borderColor = UIColor.white.cgColor
        self.yearButton.layer.cornerRadius = 5
        
        self.tableView.addSubview(self.noInboundsContainer)
        self.noInboundsContainer.translatesAutoresizingMaskIntoConstraints = false
        self.noInboundsContainer.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor).isActive = true
        self.noInboundsContainer.centerYAnchor.constraint(equalTo: self.tableView.centerYAnchor).isActive = true
        self.tableView.tableFooterView = UIView()
    }
    
    private func populate(cell: InboundCell, with screenModel: InboundScreenModel) {
        var timeString = ""
        if screenModel.fromValue == "ICN" {
            timeString = String(format: "\(screenModel.timeValue), South Korea")
        } else {
            timeString = screenModel.timeValue
        }
        cell.timeLabel.text = timeString
        cell.palletCountLabel.text = screenModel.palletCountValue
        cell.fromLabel.text = screenModel.fromValue
        cell.toLabel.text = screenModel.toValue
        cell.weightLabel.text = screenModel.weightValue
    }
    
    @objc private func refreshDataSource() {
        self.loadDataSource(delay: 1.0)
    }
    
    private func setCurrentYear() {
        let date = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: date)
        self.selectedYear = "\(currentYear)"
        self.yearButton.setTitle("\(currentYear)", for: .normal)
    }
    
    private func showRequestFailedAlert() {
        let alertController = UIAlertController(
            title: "Submission Failed",
            message: "Error occurred while submitting entry. Please try again.",
            preferredStyle: .alert
        )
        
        let okayAction = UIAlertAction(
            title: "Okay",
            style: .default,
            handler: nil
        )
        
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func sortDataSourceForSection() {
        var dataSource = [InboundSectionScreenModel]()
        
        var tempSectionModel = InboundSectionScreenModel(month: "")
        var tempDataSource = [InboundScreenModel]()
        for object in self.dataSource {
            let date = object.timeValue.toDate()
            let calendar = Calendar.current
            let month = calendar.component(.month, from: date).toStringMonth()
            if month != tempSectionModel.month {
                if tempDataSource.count > 0 {
                    tempSectionModel.dataSource = tempDataSource
                    dataSource.append(tempSectionModel)
                }
                let sectionModel = InboundSectionScreenModel(month: month)
                tempSectionModel = sectionModel
                tempDataSource = [InboundScreenModel]()
                tempDataSource.append(object)
            } else {
                tempDataSource.append(object)
            }
        }
        
        if tempDataSource.count > 0 {
            tempSectionModel.dataSource = tempDataSource
            dataSource.append(tempSectionModel)
        }
        self.sectionDataSource = dataSource
    }
    
    // MARK: - UITableViewDelegate Functions
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let month = self.sectionDataSource[section].month
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 90))
        view.backgroundColor = UIColor.ICM.lightGray
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width - 100, height: 50))
        label.text = month
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.ICM.black
        view.addSubview(label)

        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 25).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        return view
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionDataSource.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionModel = self.sectionDataSource[section]
        return sectionModel.dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let safeCell = tableView.dequeueReusableCell(withIdentifier: "InboundCell", for: indexPath)
        
        let sectionModel = self.sectionDataSource[indexPath.section]
        let screenModel = sectionModel.dataSource[indexPath.row]
        guard
            let cell = safeCell as? InboundCell
        else { return safeCell }
        
        cell.dataModel = screenModel
        self.populate(cell: cell, with: screenModel)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard
            let cell = tableView.cellForRow(at: indexPath) as? InboundCell,
            let screenModel = cell.dataModel as? InboundScreenModel
        else { return }
        
        self.performSegue(withIdentifier: "showEntryVC", sender: screenModel)
    }

    // MARK: - IBAction Functions
    
    @IBAction func addButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "InboundEntrySelection", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "AddInboudnTicketVC") as? AddInboudnTicketVC
        else { return }
        
        vc.delegate = self
        vc.year = self.selectedYear
        let window = UIWindow()
        self.presentingWindowRef = window
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3, animations: {
            window.alpha = 1
        })
    }
    
    @IBAction func yearButtonTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "InboundEntrySelection", bundle: nil)
        
        guard
            let vc = storyboard.instantiateViewController(withIdentifier: "InboundYearSelectionVC") as? InboundYearSelectionVC
        else { return }
        
        vc.delegate = self
        let window = UIWindow()
        self.presentingWindowRef = window
        window.windowLevel = window.windowLevel + 10
        window.backgroundColor = .clear
        window.rootViewController = vc
        window.alpha = 0
        window.makeKeyAndVisible()
        UIView.animate(withDuration: 0.3, animations: {
            window.alpha = 1
        })
    }
    
    // MARK: - Overlaying UIWindow Dismiss Function
    
    public func backgroundTappedToDismiss() {
        self.dismissOverlayWindow()
    }
    
    // MARK: - AddInboudnTicketVCDelegate Functions
    
    public func submitButtonTapped(with dataModel: InboundEntryModel) {
        self.dismissOverlayWindow()
        self.addInboundEntry(with: dataModel)
    }
    
    // MARK: - InboundYearSelectionVCDelegate Functions
    
    public func yearSelected(with year: String) {
        self.selectedYear = year
        self.yearButton.setTitle(year, for: .normal)
        self.loadDataSource()
        self.dismissOverlayWindow()
    }
    
    // MARK: - Navigation Functions
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == "showEntryVC" {
            guard
                let destination = segue.destination as? EntrySelectionVC,
                let screenModel = sender as? InboundScreenModel
            else { return }
            
            destination.inboundID = screenModel.IDValue
        }
    }
}
