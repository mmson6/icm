//
//  InboundCell.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class InboundCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var palletCountLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    public var dataModel: InboundScreenModel?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutViews()
    }
    
    private func layoutViews() {
//        self.containerView.layer.borderWidth = 1
//        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
//        self.containerView.layer.cornerRadius = 5
    }
}
