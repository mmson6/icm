//
//  SettingSectionHeaderCell.swift
//  ICM
//
//  Created by Mikael Son on 2/9/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class SettingSectionHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
}
