//
//  RackCVCell.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class RackCVCell: UICollectionViewCell {
    
    public var dataModel: RackScreenModel?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutViews()
    }
    
    private func layoutViews() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 5
    }
}
