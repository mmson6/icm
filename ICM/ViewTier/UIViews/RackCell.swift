//
//  RackCell.swift
//  ICM
//
//  Created by Mikael Son on 2/10/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class RackCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    public var dataModel: RackScreenModel?
}
