//
//  YearCVCell.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class YearCVCell: UICollectionViewCell {
    
    public var year: String?
    @IBOutlet weak var yearLabel: UILabel!
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutViews()
    }
    
    private func layoutViews() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 5
    }
}
