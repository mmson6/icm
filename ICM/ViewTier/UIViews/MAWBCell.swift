//
//  MAWBCell.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import UIKit

public class MAWBCell: UITableViewCell {

    @IBOutlet weak var submittedTimeLabel: UILabel!
    @IBOutlet weak var MAWBLabel: UILabel!
    @IBOutlet weak var rackNumberLabel: UILabel!
    @IBOutlet weak var palletNumberLabel: UILabel!
    @IBOutlet weak var freightCountLabel: UILabel!
    
    var dataModel: MAWBScreenModel?
}
