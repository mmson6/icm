//
//  PalletEntryDomainModel.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public struct PalletEntryDomainModel {
    private let palletNumb: String
    private let time: String
    
    init(dataModel: PalletEntryModel) {
        self.palletNumb = dataModel.palletNumbValue
        self.time = dataModel.timeValue
    }
    
    public var palletNumbValue: String {
        return self.palletNumb
    }
    
    public var timeValue: String {
        return self.time
    }
    
}
