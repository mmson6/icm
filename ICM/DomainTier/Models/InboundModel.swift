//
//  InboundModel.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation
import AWSDynamoDB

public struct InboundModel {
    private var ID: String
    private var year: String
    private var from: String
    private var to: String
    private var palletCount: String
    private var weight: String
    private var time: String
    
    init(item: [String: AWSDynamoDBAttributeValue]) {
        self.ID = item["ID"]?.s ?? item["InboundID"]?.s ?? ""
        self.year = item["Year"]?.s ?? ""
        self.from = item["From"]?.s ?? ""
        self.to = item["To"]?.s ?? ""
        self.palletCount = item["PalletCount"]?.n ?? ""
        self.weight = item["Weight"]?.n ?? ""
        self.time = item["CreatedTime"]?.s ?? ""
    }
    
    public var IDValue: String {
        return self.ID
    }
    public var yearValue: String {
        return self.year
    }
    
    public var fromValue: String {
        return self.from
    }
    
    public var toValue: String {
        return self.to
    }
    
    public var palletCountValue: String {
        return self.palletCount
    }
    
    public var weightValue: String {
        return self.weight
    }
    
    public var timeValue: String {
        return self.time
    }
}
