//
//  MAWBEntryDomainModel.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public struct MAWBEntryDomainModel {
    private let inboundID: String
    private let palletNumb: String
    private let MAWB: String
    private let freightCount: String
    private let rack: String
    private let time: String
    
    init(dataModel: MAWBEntryModel) {
        self.inboundID = dataModel.inboundIDValue
        self.palletNumb = dataModel.palletNumbValue
        self.MAWB = dataModel.MAWBValue
        self.freightCount = dataModel.freightCountStringValue
        self.rack = dataModel.rackValue
        self.time = dataModel.timeValue
    }
    
    public var inboundIDValue: String {
        return self.inboundID
    }
    
    public var MAWBValue: String {
        return self.MAWB
    }
    
    public var freightCountValue: String {
        return self.freightCount
    }
    
    public var rackValue: String {
        return self.rack
    }
    
    public var timeValue: String {
        return self.time
    }
    
    public var palletNumbValue: String {
        return self.palletNumb
    }

}
