//
//  RackModel.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation
import AWSDynamoDB

public struct RackModel {
    private var name: String
    private var info: String
    
    init(item: [String: AWSDynamoDBAttributeValue]) {
        self.name = item["Name"]?.s ?? ""
        self.info = item["Info"]?.s ?? ""
    }
    
    public var nameValue: String {
        return self.name
    }
    
    public var infoValue: String {
        return self.info
    }
}
