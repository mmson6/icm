//
//  FullDetailModel.swift
//  ICM
//
//  Created by Mikael Son on 2/21/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation
import AWSDynamoDB


public struct FullDetailModel {
    private var year: String
    private var from: String
    private var to: String
    private var palletCount: String
    private var weight: String
    private var createdTime: String
    private var inboundID: String
    private var MAWB: String
    private var rackNumber: String
    private var palletNumber: String
    private var freightCount: String
    private var submittedTime: String

    
    init(item: [String: AWSDynamoDBAttributeValue]) {
        self.inboundID = item["InboundID"]?.s ?? ""
        self.year = item["Year"]?.s ?? ""
        self.from = item["From"]?.s ?? ""
        self.to = item["To"]?.s ?? ""
        self.palletCount = item["PalletCount"]?.n ?? ""
        self.weight = item["Weight"]?.n ?? ""
        self.createdTime = item["CreatedTime"]?.s ?? ""
        self.MAWB = item["MAWB"]?.s ?? ""
        self.rackNumber = item["RackNumber"]?.s ?? ""
        self.palletNumber = item["PalletNumber"]?.s ?? ""
        self.freightCount = item["FreightCount"]?.s ?? ""
        self.submittedTime = item["SubmissionTime"]?.s ?? ""
    }
    
    
    public var yearValue: String {
        return self.year
    }
    
    public var fromValue: String {
        return self.from
    }
    
    public var toValue: String {
        return self.to
    }
    
    public var palletCountValue: String {
        return self.palletCount
    }
    
    public var weightValue: String {
        return self.weight
    }
    
    public var createdTimeValue: String {
        return self.createdTime
    }
    public var inboundIDValue: String {
        return self.inboundID
    }
    public var MAWBValue: String {
        return self.MAWB
    }
    
    public var rackNumberValue: String {
        return self.rackNumber
    }
    
    public var palletNumberValue: String {
        return self.palletNumber
    }
    
    public var freightCountValue: String {
        return self.freightCount
    }
    
    public var submittedTimeValue: String {
        return self.submittedTime
    }
}
