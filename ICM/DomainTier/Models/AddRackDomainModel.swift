//
//  AddRackDomainModel.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public class AddRackDomainModel {
    private var name: String
    private var info: String
    
    init(dataModel: AddRackModel) {
        self.name = dataModel.nameValue
        self.info = dataModel.infoValue
    }
    
    public var nameValue: String {
        return self.name
    }
    
    public var infoValue: String {
        return self.info
    }
}
