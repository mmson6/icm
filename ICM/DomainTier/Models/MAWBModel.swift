//
//  MAWBModel.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation
import AWSDynamoDB

public struct MAWBModel {
    private var inboundID: String
    private var MAWB: String
    private var rackNumber: String
    private var palletNumber: String
    private var freightCount: String
    private var submittedTime: String
    
    init(item: [String: AWSDynamoDBAttributeValue]) {
        self.inboundID = item["InboundID"]?.s ?? ""
        self.MAWB = item["MAWB"]?.s ?? ""
        self.rackNumber = item["RackNumber"]?.s ?? ""
        self.palletNumber = item["PalletNumber"]?.s ?? ""
        self.freightCount = item["FreightCount"]?.s ?? ""
        self.submittedTime = item["SubmissionTime"]?.s ?? ""
    }
    
    public var inboundIDValue: String {
        return self.inboundID
    }
    public var MAWBValue: String {
        return self.MAWB
    }
    
    public var rackNumberValue: String {
        return self.rackNumber
    }
    
    public var palletNumberValue: String {
        return self.palletNumber
    }
    
    public var freightCountValue: String {
        return self.freightCount
    }
    
    public var submittedTimeValue: String {
        return self.submittedTime
    }
}
