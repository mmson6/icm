//
//  InboundEntryDomainModel.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct InboundEntryDomainModel {
    private let ID: String
    private let year: String
    private let palletCount: String
    private let weight: String
    private let from: String
    private let to: String
    private let time: String
    
    
    init(dataModel: InboundEntryModel) {
        self.ID = dataModel.IDValue
        self.year = dataModel.yearValue
        self.palletCount = dataModel.palletCountValue
        self.weight = dataModel.weightValue
        self.from = dataModel.fromValue
        self.to = dataModel.toValue
        self.time = dataModel.timeValue
    }
    
    public var IDValue: String {
        return self.ID
    }
    
    public var yearValue: String {
        return self.year
    }
    
    public var palletCountValue: String {
        return self.palletCount
    }
    
    public var weightValue: String {
        return self.weight
    }
    
    public var fromValue: String {
        return self.from
    }
    
    public var toValue: String {
        return self.to
    }
    
    public var timeValue: String {
        return self.time
    }
}
