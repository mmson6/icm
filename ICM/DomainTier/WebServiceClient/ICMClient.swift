//
//  ICMClient.swift
//  ICM
//
//  Created by Mikael Son on 2/8/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import AWSCore
import AWSDynamoDB

import Foundation

fileprivate let InboundsTable = "icm-mobilehub-1380612440-Inbounds"
fileprivate let MAWBsTable = "icm-mobilehub-1380612440-MAWBs"
fileprivate let RacksTable = "icm-mobilehub-1380612440-Racks"
fileprivate let MAWBsDataStackTable = "icm-mobilehub-1380612440-MAWBsDataStack"
fileprivate let InboundIDIndex = "InboundID-index"
fileprivate let RackNumberIndex = "RackNumber-index"
fileprivate let PalletNumberIndex = "PalletNumber-index"


internal protocol ICMClient {
    func addInbound(domainModel: InboundEntryDomainModel) -> FutureResult<Bool>
    func addRack(domainModel: AddRackDomainModel) -> FutureResult<Bool>
    func deleteRack(domainModel: DeleteRackDomainModel) -> FutureResult<Bool>
    func readDetailsByMAWB(_ MAWBValue: String?) -> FutureResult<[FullDetailModel]>
    func readInbounds(for year: String?) -> FutureResult<[InboundModel]>
    func readInboundsByMAWB(_ MAWBValue: String?) -> FutureResult<[InboundModel]>
    func readInboundsByPallet(_ palletValue: String?) -> FutureResult<[InboundModel]>
    func readInboundsByRack(_ rackValue: String?) -> FutureResult<[InboundModel]>
    func readMAWBWithMAWB(_ MAWBValue: String?) -> FutureResult<[MAWBModel]>
    func readMAWBsByInbound(with inboundID: String?) -> FutureResult<[MAWBModel]>
    func readMAWBsByPallet(with palletNumberValue: String?) -> FutureResult<[MAWBModel]>
    func readMAWBsByRack(with rackNumberValue: String?) -> FutureResult<[MAWBModel]>
    func readRacks() -> FutureResult<[RackModel]>
    func submitMAWBEntry(domainModel: MAWBEntryDomainModel) -> FutureResult<Bool>
    func submitPalletEntry(domainModel: PalletEntryDomainModel) -> FutureResult<Bool>
}

fileprivate let sharedNetworkClient = NetworkICMClient()

internal class NetworkICMClient: ICMClient {
    
    public static let shared: NetworkICMClient = sharedNetworkClient
    
    // MARK: - ICMClient
    
    internal func addInbound(domainModel: InboundEntryDomainModel) -> FutureResult<Bool> {
        let deferred = DeferredResult<Bool>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let putItemInput = AWSDynamoDBPutItemInput(),
            let id = AWSDynamoDBAttributeValue(),
            let year = AWSDynamoDBAttributeValue(),
            let palletCount = AWSDynamoDBAttributeValue(),
            let weight = AWSDynamoDBAttributeValue(),
            let from = AWSDynamoDBAttributeValue(),
            let to = AWSDynamoDBAttributeValue(),
            let time = AWSDynamoDBAttributeValue() {
            
            putItemInput.tableName = InboundsTable
            id.s = domainModel.IDValue
            year.s = domainModel.yearValue
            palletCount.n = domainModel.palletCountValue
            weight.n = domainModel.weightValue
            from.s = domainModel.fromValue
            to.s = domainModel.toValue
            time.s = domainModel.timeValue
            
            putItemInput.item = [
                "ID" : id,
                "Year" : year,
                "CreatedTime" : time,
                "From" : from,
                "To" : to,
                "PalletCount" : palletCount,
                "Weight" : weight,
            ]
            
            dynamoDB.putItem(putItemInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    deferred.success(value: true)
                }
            })
        }
        
        return deferred
    }
    internal func addRack(domainModel: AddRackDomainModel) -> FutureResult<Bool> {
        let deferred = DeferredResult<Bool>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let putItemInput = AWSDynamoDBPutItemInput(),
            let name = AWSDynamoDBAttributeValue(),
            let info = AWSDynamoDBAttributeValue()
        {
            putItemInput.tableName = RacksTable
            name.s = domainModel.nameValue
            info.s = domainModel.infoValue
            
            // dynamoDB Attribute cannot contain empty string
            if domainModel.infoValue == "" {
                info.s = " "
            }
            
            putItemInput.item = [
                "Name" : name,
                "Info" : info
            ]
            
            dynamoDB.putItem(putItemInput) { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    deferred.success(value: true)
                }
            }
        } else {
            deferred.failure(error: ICMError.requestCouldNotBeCompleted)
        }
        
        return deferred
    }
    
    internal func deleteRack(domainModel: DeleteRackDomainModel) -> FutureResult<Bool> {
        let deferred = DeferredResult<Bool>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let deleteItemInput = AWSDynamoDBDeleteItemInput(),
            let name = AWSDynamoDBAttributeValue() {
            
            deleteItemInput.tableName = RacksTable

            name.s = domainModel.nameValue
            deleteItemInput.key = ["Name": name]
            
            dynamoDB.deleteItem(deleteItemInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    deferred.success(value: true)
                }
            })
        } else {
            deferred.failure(error: ICMError.requestCouldNotBeCompleted)
        }
        
        return deferred
    }
    
    internal func readDetailsByMAWB(_ MAWBValue: String?) -> FutureResult<[FullDetailModel]> {
        let deferred = DeferredResult<[FullDetailModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let MAWB = AWSDynamoDBAttributeValue(),
            let MAWBValue = MAWBValue {
            queryInput.tableName = MAWBsDataStackTable
            queryInput.keyConditionExpression = "#MAWB = :MAWB"
            
            MAWB.s = MAWBValue
            
            queryInput.expressionAttributeNames = [
                "#MAWB": "MAWB"
            ]
            
            queryInput.expressionAttributeValues = [
                ":MAWB": MAWB
            ]
            
            queryInput.returnConsumedCapacity = .total
            queryInput.consistentRead = true
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- Details Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [FullDetailModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = FullDetailModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readInbounds(for yearValue: String?) -> FutureResult<[InboundModel]> {
        let deferred = DeferredResult<[InboundModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let year = AWSDynamoDBAttributeValue(),
            let yearValue = yearValue {
            queryInput.tableName = InboundsTable
            queryInput.keyConditionExpression = "#Year = :year"
            
            year.s = yearValue
            
            queryInput.expressionAttributeNames = [
                "#Year": "Year"
            ]
            
            queryInput.expressionAttributeValues = [
                ":year": year
            ]
            
            queryInput.returnConsumedCapacity = .total
            queryInput.consistentRead = true

            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- Inbounds Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [InboundModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = InboundModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readInboundsByMAWB(_ MAWBValue: String?) -> FutureResult<[InboundModel]> {
        let deferred = DeferredResult<[InboundModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let MAWB = AWSDynamoDBAttributeValue(),
            let MAWBValue = MAWBValue {
            queryInput.tableName = MAWBsDataStackTable
            queryInput.keyConditionExpression = "#MAWB = :MAWB"
            
            MAWB.s = MAWBValue
            
            queryInput.expressionAttributeNames = [
                "#MAWB": "MAWB"
            ]
            
            queryInput.expressionAttributeValues = [
                ":MAWB": MAWB
            ]
            
            queryInput.returnConsumedCapacity = .total
            queryInput.consistentRead = true
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- Inbounds Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [InboundModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = InboundModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readInboundsByPallet(_ palletValue: String?) -> FutureResult<[InboundModel]> {
        let deferred = DeferredResult<[InboundModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let pallet = AWSDynamoDBAttributeValue(),
            let palletValue = palletValue {
            queryInput.tableName = MAWBsDataStackTable
            queryInput.indexName = PalletNumberIndex
            queryInput.keyConditionExpression = "#PalletNumber = :palletNumber"
            
            pallet.s = palletValue
            
            queryInput.expressionAttributeNames = [
                "#PalletNumber": "PalletNumber"
            ]
            
            queryInput.expressionAttributeValues = [
                ":palletNumber": pallet
            ]
            
            queryInput.returnConsumedCapacity = .total
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- Inbounds Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [InboundModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = InboundModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readInboundsByRack(_ rackValue: String?) -> FutureResult<[InboundModel]> {
        let deferred = DeferredResult<[InboundModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let rack = AWSDynamoDBAttributeValue(),
            let rackValue = rackValue {
            queryInput.tableName = MAWBsDataStackTable
            queryInput.indexName = RackNumberIndex
            queryInput.keyConditionExpression = "#RackNumber = :rackNumber"
            
            rack.s = rackValue
            
            queryInput.expressionAttributeNames = [
                "#RackNumber": "RackNumber"
            ]
            
            queryInput.expressionAttributeValues = [
                ":rackNumber": rack
            ]
            
            queryInput.returnConsumedCapacity = .total
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- Inbounds Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [InboundModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = InboundModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readMAWBWithMAWB(_ MAWBValue: String?) -> FutureResult<[MAWBModel]> {
        let deferred = DeferredResult<[MAWBModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let MAWB = AWSDynamoDBAttributeValue(),
            let MAWBValue = MAWBValue {
            queryInput.tableName = MAWBsTable
            queryInput.keyConditionExpression = "#MAWB = :MAWB"
            MAWB.s = MAWBValue
            
            queryInput.expressionAttributeNames = [
                "#MAWB": "MAWB"
            ]
            
            queryInput.expressionAttributeValues = [
                ":MAWB": MAWB
            ]
            
            queryInput.returnConsumedCapacity = .total
            queryInput.consistentRead = true
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- MAWBs Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [MAWBModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = MAWBModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readMAWBsByInbound(with inboundIDValue: String?) -> FutureResult<[MAWBModel]> {
        let deferred = DeferredResult<[MAWBModel]>()
        let dynamoDB = AWSDynamoDB.default()

        if let queryInput = AWSDynamoDBQueryInput(),
            let inboundID = AWSDynamoDBAttributeValue(),
            let inboundIDValue = inboundIDValue {
            queryInput.tableName = MAWBsTable
            queryInput.indexName = InboundIDIndex
            queryInput.keyConditionExpression = "#inboundID = :inboundID"
            inboundID.s = inboundIDValue

            queryInput.expressionAttributeNames = [
                "#inboundID": "InboundID"
            ]

            queryInput.expressionAttributeValues = [
                ":inboundID": inboundID
            ]

            queryInput.returnConsumedCapacity = .total

            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- MAWBs Read Query Consumed: \(consumedCapacity) -----------")
                    }

                    var dataModels = [MAWBModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = MAWBModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readMAWBsByPallet(with palletNumberValue: String?) -> FutureResult<[MAWBModel]> {
        let deferred = DeferredResult<[MAWBModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let palletNumber = AWSDynamoDBAttributeValue(),
            let palletNumberValue = palletNumberValue {
            queryInput.tableName = MAWBsTable
            queryInput.indexName = PalletNumberIndex
            queryInput.keyConditionExpression = "#palletNumber = :palletNumber"
            palletNumber.s = palletNumberValue
            
            queryInput.expressionAttributeNames = [
                "#palletNumber": "PalletNumber"
            ]
            
            queryInput.expressionAttributeValues = [
                ":palletNumber": palletNumber
            ]
            
            queryInput.returnConsumedCapacity = .total
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- MAWBs Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [MAWBModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = MAWBModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readMAWBsByRack(with rackNumberValue: String?) -> FutureResult<[MAWBModel]> {
        let deferred = DeferredResult<[MAWBModel]>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let queryInput = AWSDynamoDBQueryInput(),
            let rackNumber = AWSDynamoDBAttributeValue(),
            let rackNumberValue = rackNumberValue {
            queryInput.tableName = MAWBsTable
            queryInput.indexName = RackNumberIndex
            queryInput.keyConditionExpression = "#rackNumber = :rackNumber"
            rackNumber.s = rackNumberValue
            
            queryInput.expressionAttributeNames = [
                "#rackNumber": "RackNumber"
            ]
            
            queryInput.expressionAttributeValues = [
                ":rackNumber": rackNumber
            ]
            
            queryInput.returnConsumedCapacity = .total
            
            dynamoDB.query(queryInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- MAWBs Read Query Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [MAWBModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = MAWBModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
        
        return deferred
    }
    
    internal func readRacks() -> FutureResult<[RackModel]> {
        let deferred = DeferredResult<[RackModel]>()
        let dynamoDB = AWSDynamoDB.default()
        if let scanInput = AWSDynamoDBScanInput() {
            scanInput.tableName = RacksTable
            scanInput.returnConsumedCapacity = .total
            scanInput.consistentRead = true
            
            dynamoDB.scan(scanInput, completionHandler: { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    if let consumedCapacity = output?.consumedCapacity?.capacityUnits {
                        NSLog("----------- Racks Read Scan Consumed: \(consumedCapacity) -----------")
                    }
                    
                    var dataModels = [RackModel]()
                    if let output = output, let items = output.items {
                        for item in items {
                            let dataModel = RackModel(item: item)
                            dataModels.append(dataModel)
                        }
                    }
                    deferred.success(value: dataModels)
                }
            })
        }
 
        return deferred
    }
    
    internal func submitMAWBEntry(domainModel: MAWBEntryDomainModel) -> FutureResult<Bool> {
        let deferred = DeferredResult<Bool>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let putItemInput = AWSDynamoDBPutItemInput(),
            let inboundID = AWSDynamoDBAttributeValue(),
            let palletNumb = AWSDynamoDBAttributeValue(),
            let MAWB = AWSDynamoDBAttributeValue(),
            let freightCount = AWSDynamoDBAttributeValue(),
            let rack = AWSDynamoDBAttributeValue(),
            let time = AWSDynamoDBAttributeValue()
        {
            putItemInput.tableName = MAWBsTable
            inboundID.s = domainModel.inboundIDValue
            palletNumb.s = domainModel.palletNumbValue
            MAWB.s = domainModel.MAWBValue
            freightCount.s = domainModel.freightCountValue
            rack.s = domainModel.rackValue
            time.s = domainModel.timeValue
            
            putItemInput.item = [
                "InboundID": inboundID,
                "MAWB" : MAWB,
                "RackNumber" : rack,
                "FreightCount" : freightCount,
                "PalletNumber" : palletNumb,
                "SubmissionTime" : time,
            ]
            
            dynamoDB.putItem(putItemInput) { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                } else {
                    deferred.success(value: true)
                }
            }
        } else {
            deferred.failure(error: ICMError.requestCouldNotBeCompleted)
        }
        
        return deferred
    }
    
    internal func submitPalletEntry(domainModel: PalletEntryDomainModel) -> FutureResult<Bool> {
        let deferred = DeferredResult<Bool>()
        let dynamoDB = AWSDynamoDB.default()
        
        if let putItemInput = AWSDynamoDBPutItemInput(),
            let palletNumb = AWSDynamoDBAttributeValue(),
            let time = AWSDynamoDBAttributeValue()
        {
            putItemInput.tableName = "TBD"
            palletNumb.s = domainModel.palletNumbValue
            time.s = domainModel.timeValue
            
            putItemInput.item = [
                "PalletNumber" : palletNumb,
                "CreatedTime" : time
            ]
            
            dynamoDB.putItem(putItemInput) { (output, error) in
                if let error = error {
                    deferred.failure(error: error)
                }
                deferred.success(value: true)
            }
        } else {
            deferred.failure(error: ICMError.requestCouldNotBeCompleted)
        }
        
        return deferred
    }
}


// Batch Write Example
/*
 for j in 1...32 {
 var writeRequests = [AWSDynamoDBWriteRequest]()
 for i in 1...25 {
 if let writeRequest = AWSDynamoDBWriteRequest(),
 let putRequest = AWSDynamoDBPutRequest(),
 let name = AWSDynamoDBAttributeValue(),
 let info = AWSDynamoDBAttributeValue()
 {
 name.s = "name \(j)-\(i)"
 info.s = "info \(j)-\(i)"
 putRequest.item = [
 "Name": name,
 "Info": info
 ]
 writeRequest.putRequest = putRequest
 writeRequests.append(writeRequest)
 }
 }
 
 if let batchInput = AWSDynamoDBBatchWriteItemInput() {
 batchInput.requestItems = [
 RacksTable : writeRequests
 ]
 
 dynamoDB.batchWriteItem(batchInput, completionHandler: { (output, error) in
 if let error = error {
 deferred.failure(error: error)
 
 NSLog("tehahfw rror \(error)")
 } else {
 NSLog("---------------------")
 }
 })
 }
 }
 
 deferred.success(value: true)
 
 */










//        let mapper = AWSDynamoDBObjectMapper.default()
//
//        let queryExpression = AWSDynamoDBQueryExpression()
//        queryExpression.keyConditionExpression = "#name = :name"
//        queryExpression.expressionAttributeNames = [
//            "#name": "Name"
//        ]
//        queryExpression.expressionAttributeValues = [
//            ":name": "test2"
//        ]
//
//        mapper.query(<#T##resultClass: AnyClass##AnyClass#>, expression: queryExpression) { (output, error) in
//            if let error = error {
//                print("Error occurred")
//            } else {
//
//            }
//        }


//
//
//        if let putItemInput = AWSDynamoDBPutItemInput(),
//            let name = AWSDynamoDBAttributeValue(),
//            let info = AWSDynamoDBAttributeValue()
//        {
//            putItemInput.tableName = RacksTable
//            name.s = domainModel.nameValue
//            info.s = domainModel.infoValue
//
//            // dynamoDB Attribute cannot contain empty string
//            if domainModel.infoValue == "" {
//                info.s = " "
//            }
//
//            putItemInput.item = [
//                "Name" : name,
//                "Info" : info
//            ]
//
//            dynamoDB.putItem(putItemInput) { (output, error) in
//                if let error = error {
//                    deferred.failure(error: error)
//                } else {
//                    deferred.success(value: true)
//                }
//            }
//        } else {
//            deferred.failure(error: ICMError.requestCouldNotBeCompleted)
//        }
//









// Query with global index
/*

 if let query = AWSDynamoDBQueryInput(),
 let rackNumb = AWSDynamoDBAttributeValue() {
 query.tableName = MAWBsTable
 query.indexName = "RackNumber-index"
 query.keyConditionExpression = "#rackNumber = :rackNumber"
 
 rackNumb.s = "Rack 1"
 query.expressionAttributeNames = [
 "#rackNumber": "RackNumber"
 ]
 query.expressionAttributeValues = [
 ":rackNumber": rackNumb
 ]
 //            query.projectionExpression = "MAWB"
 dynamoDB.query(query, completionHandler: { (output, error) in
 if let error = error {
 deferred.failure(error: error)
 } else {
 print(output)
 }
 })
 }
 
*/
