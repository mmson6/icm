//
//  FullDetailScreenModel.swift
//  ICM
//
//  Created by Mikael Son on 2/21/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public struct FullDetailScreenModel {
    private var year: String
    private var from: String
    private var to: String
    private var palletCount: String
    private var weight: String
    private var createdTime: String
    private var inboundID: String
    private var MAWB: String
    private var rackNumber: String
    private var palletNumber: String
    private var freightCount: String
    private var submittedTime: String
    
    
    init(dataModel: FullDetailModel) {
        self.year = dataModel.yearValue
        self.from = dataModel.fromValue
        self.to = dataModel.toValue
        self.palletCount = dataModel.palletCountValue
        self.weight = dataModel.weightValue
        self.createdTime = dataModel.createdTimeValue
        self.inboundID = dataModel.inboundIDValue
        self.MAWB = dataModel.MAWBValue
        self.rackNumber = dataModel.rackNumberValue
        self.palletNumber = dataModel.palletNumberValue
        self.freightCount = dataModel.freightCountValue
        self.submittedTime = dataModel.submittedTimeValue
    }
    
    
    public var yearValue: String {
        return self.year
    }
    
    public var fromValue: String {
        return self.from
    }
    
    public var toValue: String {
        return self.to
    }
    
    public var palletCountValue: String {
        return self.palletCount
    }
    
    public var weightValue: String {
        return self.weight
    }
    
    public var createdTimeValue: String {
        return self.createdTime
    }
    public var inboundIDValue: String {
        return self.inboundID
    }
    public var MAWBValue: String {
        return self.MAWB
    }
    
    public var rackNumberValue: String {
        return self.rackNumber
    }
    
    public var palletNumberValue: String {
        return self.palletNumber
    }
    
    public var freightCountValue: String {
        return self.freightCount
    }
    
    public var submittedTimeValue: String {
        return self.submittedTime
    }
}
