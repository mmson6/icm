//
//  MAWBEntryModel.swift
//  ICM
//
//  Created by Mikael Son on 2/13/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct MAWBEntryModel {
    private let inboundID: String
    private let palletNumb: String
    private let MAWB: String
    private let freightCount: Int
    private let rack: String
    private let time: String
    
    init(inboundID: String, pallet: String, MAWB: String , freightCount: Int, rack: String, time: String) {
        self.inboundID = inboundID
        self.palletNumb = pallet
        self.MAWB = MAWB
        self.freightCount = freightCount
        self.rack = rack
        self.time = time
    }
    
    public var inboundIDValue: String {
        return self.inboundID
    }
    
    public var MAWBValue: String {
        return self.MAWB
    }
    
    public var freightCountIntValue: Int {
        return self.freightCount
    }
    
    public var freightCountStringValue: String {
        return "\(self.freightCount)"
    }
    
    public var rackValue: String {
        return self.rack
    }
    
    public var timeValue: String {
        return self.time
    }
    
    public var palletNumbValue: String {
        return self.palletNumb
    }
}
