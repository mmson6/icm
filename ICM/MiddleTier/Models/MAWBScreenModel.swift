//
//  MAWBScreenModel.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public struct MAWBScreenModel {
    private var inboundID: String
    private var MAWB: String
    private var rackNumber: String
    private var palletNumber: String
    private var freightCount: String
    private var submittedTime: String
    
    init(dataModel: MAWBModel) {
        self.inboundID = dataModel.inboundIDValue
        self.MAWB = dataModel.MAWBValue
        self.rackNumber = dataModel.rackNumberValue
        self.palletNumber = dataModel.palletNumberValue
        self.freightCount = dataModel.freightCountValue
        self.submittedTime = dataModel.submittedTimeValue
    }
    
    public var inboundIDValue: String {
        return self.inboundID
    }
    public var MAWBValue: String {
        return self.MAWB
    }
    
    public var rackNumberValue: String {
        return self.rackNumber
    }
    
    public var palletNumberValue: String {
        return self.palletNumber
    }
    
    public var freightCountValue: String {
        return self.freightCount
    }
    
    public var submittedTimeValue: String {
        return self.submittedTime
    }
}
