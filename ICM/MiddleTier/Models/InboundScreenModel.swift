//
//  InboundScreenModel.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct InboundScreenModel {
    private var ID: String
    private var year: String
    private var from: String
    private var to: String
    private var palletCount: String
    private var weight: String
    private var time: String
    
    init(dataModel: InboundModel) {
        self.ID = dataModel.IDValue
        self.year = dataModel.yearValue
        self.from = dataModel.fromValue
        self.to = dataModel.toValue
        self.palletCount = dataModel.palletCountValue
        self.weight = dataModel.weightValue
        self.time = dataModel.timeValue
    }
    
    public var IDValue: String {
        return self.ID
    }
    
    public var yearValue: String {
        return self.year
    }
    
    public var fromValue: String {
        return self.from
    }
    
    public var toValue: String {
        return self.to
    }
    
    public var palletCountValue: String {
        return self.palletCount
    }
    
    public var weightValue: String {
        return self.weight
    }
    
    public var timeValue: String {
        return self.time
    }
}
