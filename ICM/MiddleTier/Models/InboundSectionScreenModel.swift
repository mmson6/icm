//
//  InboundSectionScreenModel.swift
//  ICM
//
//  Created by Mikael Son on 2/18/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct InboundSectionScreenModel {
    public let month: String
    public var dataSource = [InboundScreenModel]()
    
    init(month: String) {
        self.month = month
    }
}
