//
//  InboundEntryModel.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct InboundEntryModel {
    private let ID: String
    private let year: String
    private let palletCount: String
    private let weight: String
    private let from: String
    private let to: String
    private let time: String
    
    init(id: String, year: String, palletCount: String, weight: String , from: String, to: String, time: String) {
        self.ID = id
        self.year = year
        self.palletCount = palletCount
        self.weight = weight
        self.from = from
        self.to = to
        self.time = time
    }
    
    public var IDValue: String {
        return self.ID
    }
    
    public var yearValue: String {
        return self.year
    }
    
    public var palletCountValue: String {
        return self.palletCount
    }
    
    public var weightValue: String {
        return self.weight
    }
    
    public var fromValue: String {
        return self.from
    }
    
    public var toValue: String {
        return self.to
    }
    
    public var timeValue: String {
        return self.time
    }
}
