//
//  AddRackModel.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct AddRackModel {
    private var name: String
    private var info: String
    
    init(name: String, info: String) {
        self.name = name
        self.info = info
    }
    
    public var nameValue: String {
        return self.name
    }
    
    public var infoValue: String {
        return self.info
    }
}
