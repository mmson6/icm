//
//  PalletEntryModel.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public struct PalletEntryModel {
    private let palletNumb: String
    private let time: String
    
    init(pallet: String, time: String) {
        self.palletNumb = pallet
        self.time = time
    }
    
    public var palletNumbValue: String {
        return self.palletNumb
    }
    
    public var timeValue: String {
        return self.time
    }
    
}
