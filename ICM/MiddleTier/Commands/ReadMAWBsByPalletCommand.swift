//
//  ReadMAWBsByPalletCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/19/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public class ReadMAWBsByPalletCommand {
    
    private var readMAWBs: ((String?) -> FutureResult<[MAWBModel]>)
    
    internal init(MAWBReader: @escaping (String?) -> FutureResult<[MAWBModel]>) {
        self.readMAWBs = MAWBReader
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let readMAWBs = client.readMAWBsByPallet
        self.init(MAWBReader: readMAWBs)
    }
    
    public func execute(with palletNumber: String?) -> FutureResult<[MAWBScreenModel]> {
        let readMAWBs = self.readMAWBs(palletNumber)
            .pipe(into: self.convertToScreenModels(dataModels:))
        return readMAWBs
    }
    
    private func convertToScreenModels(dataModels: [MAWBModel]) -> FutureResult<[MAWBScreenModel]> {
        let deferred = DeferredResult<[MAWBScreenModel]>()
        let screenModels = dataModels.map { MAWBScreenModel(dataModel: $0) }
        let sortedScreenModels = screenModels.sorted { $0.submittedTimeValue.toDate() > $1.submittedTimeValue.toDate() }
        deferred.success(value: sortedScreenModels)
        
        return deferred
    }
}
