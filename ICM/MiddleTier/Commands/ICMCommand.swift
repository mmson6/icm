//
//  ICMCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public func createArrayObjectDataSource<T>(withScreenModels models: [T]) -> FutureResult<ObjectDataSource<T>> {
    let deferred = DeferredResult<ObjectDataSource<T>>()
    let dataSource = ArrayObjectDataSource(objects: models)
    deferred.success(value: dataSource)
    return deferred
}
