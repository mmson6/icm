//
//  DeleteRackCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public class DeleteRackCommand {
    
    private let deleteRack: ((DeleteRackDomainModel) -> FutureResult<Bool>)
    
    internal init(rackRemover: @escaping ((DeleteRackDomainModel) -> FutureResult<Bool>)) {
        self.deleteRack = rackRemover
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let deleteRack = client.deleteRack
        self.init(rackRemover: deleteRack)
    }
    
    public func execute(dataModel: DeleteRackModel) -> FutureResult<Bool> {
        let deleteRack = self.convertToDomainModel(dataModel: dataModel)
        .pipe(into: self.deleteRack)
        
        return deleteRack
    }
    
    private func convertToDomainModel(dataModel: DeleteRackModel) -> FutureResult<DeleteRackDomainModel> {
        let deferred = DeferredResult<DeleteRackDomainModel>()
        let domainModel = DeleteRackDomainModel(dataModel: dataModel)
        deferred.success(value: domainModel)
        return deferred
    }
}
