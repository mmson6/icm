//
//  SubmitMAWBEntryCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public class SubmitMAWBEntryCommand {
    
    private let submitMAWBEntry: ((MAWBEntryDomainModel) -> FutureResult<Bool>)
    
    internal init(MAWBEntrySubmit: @escaping (MAWBEntryDomainModel) -> FutureResult<Bool>) {
        self.submitMAWBEntry = MAWBEntrySubmit
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let entrySubmitter = client.submitMAWBEntry
        self.init(MAWBEntrySubmit: entrySubmitter)
    }
    
    public func execute(dataModel: MAWBEntryModel) -> FutureResult<Bool> {
        let submitEntry = self.convertToDomainModel(dataModel: dataModel)
        .pipe(into: self.submitMAWBEntry)

        return submitEntry
    }
    
    private func convertToDomainModel(dataModel: MAWBEntryModel) -> FutureResult<MAWBEntryDomainModel> {
        let deferred = DeferredResult<MAWBEntryDomainModel>()
        let domainModel = MAWBEntryDomainModel(dataModel: dataModel)
        deferred.success(value: domainModel)
        return deferred
    }
}
