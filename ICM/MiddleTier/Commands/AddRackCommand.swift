//
//  AddRackCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public class AddRackCommand {
    
    private let addRack: ((AddRackDomainModel) -> FutureResult<Bool>)
    
    internal init(rackAdder: @escaping ((AddRackDomainModel) -> FutureResult<Bool>)) {
        self.addRack = rackAdder
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let rackAdder = client.addRack
        self.init(rackAdder: rackAdder)
    }
    
    public func execute(dataModel: AddRackModel) -> FutureResult<Bool> {
        let addRack = self.convertToDomainModel(dataModel: dataModel)
        .pipe(into: self.addRack)
        
        return addRack
    }
    
    private func convertToDomainModel(dataModel: AddRackModel) -> FutureResult<AddRackDomainModel> {
        let deferred = DeferredResult<AddRackDomainModel>()
        let domainModel = AddRackDomainModel(dataModel: dataModel)
        deferred.success(value: domainModel)
        return deferred
    }
}
