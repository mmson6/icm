//
//  SubmitInboundTicketCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public class SubmitInboundTicketCommand {
    
    private let addInbound: ((InboundEntryDomainModel) -> FutureResult<Bool>)
    
    internal init(inboundAdder: @escaping ((InboundEntryDomainModel) -> FutureResult<Bool>)) {
        self.addInbound = inboundAdder
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let inboundAdder = client.addInbound
        self.init(inboundAdder: inboundAdder)
    }
    
    public func execute(dataModel: InboundEntryModel) -> FutureResult<Bool> {
        let addInbound = self.convertToDomainModel(dataModel: dataModel)
            .pipe(into: self.addInbound)
        
        return addInbound
    }
    
    private func convertToDomainModel(dataModel: InboundEntryModel) -> FutureResult<InboundEntryDomainModel> {
        let deferred = DeferredResult<InboundEntryDomainModel>()
        let domainModel = InboundEntryDomainModel(dataModel: dataModel)
        deferred.success(value: domainModel)
        return deferred
    }
}
