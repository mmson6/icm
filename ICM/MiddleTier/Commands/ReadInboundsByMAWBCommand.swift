//
//  ReadInboundsByMAWBCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/20/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public class ReadInboundsByMAWBCommand {
    
    private var readInbounds: ((String?) -> FutureResult<[InboundModel]>)
    
    internal init(inboundReader: @escaping (String?) -> FutureResult<[InboundModel]>) {
        self.readInbounds = inboundReader
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let readInbounds = client.readInboundsByMAWB
        self.init(inboundReader: readInbounds)
    }
    
    public func execute(with MAWB: String?) -> FutureResult<[InboundScreenModel]> {
        let readInbounds = self.readInbounds(MAWB)
            .pipe(into: self.convertToScreenModels(dataModels:))
        return readInbounds
    }
    
    private func convertToScreenModels(dataModels: [InboundModel]) -> FutureResult<[InboundScreenModel]> {
        let deferred = DeferredResult<[InboundScreenModel]>()
        let screenModels = dataModels.map { InboundScreenModel(dataModel: $0) }
        let sortedScreenModels = screenModels.sorted { $0.timeValue.toDate() > $1.timeValue.toDate() }
        deferred.success(value: sortedScreenModels)
        
        return deferred
    }
}

