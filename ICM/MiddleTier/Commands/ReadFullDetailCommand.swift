//
//  ReadFullDetailCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/21/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public class ReadFullDetailCommand {
    
    private var readDetails: ((String?) -> FutureResult<[FullDetailModel]>)
    
    internal init(detailReader: @escaping (String?) -> FutureResult<[FullDetailModel]>) {
        self.readDetails = detailReader
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let readDetails = client.readDetailsByMAWB
        self.init(detailReader: readDetails)
    }
    
    public func execute(with MAWB: String?) -> FutureResult<[FullDetailScreenModel]> {
        let readDetails = self.readDetails(MAWB)
            .pipe(into: self.convertToScreenModels(dataModels:))
        return readDetails
    }
    
    private func convertToScreenModels(dataModels: [FullDetailModel]) -> FutureResult<[FullDetailScreenModel]> {
        let deferred = DeferredResult<[FullDetailScreenModel]>()
        let screenModels = dataModels.map { FullDetailScreenModel(dataModel: $0) }
        deferred.success(value: screenModels)
        
        return deferred
    }
}

