//
//  ReadMAWBsByInboundCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/16/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public class ReadMAWBsByInboundCommand {
    
    private var readMAWBs: ((String?) -> FutureResult<[MAWBModel]>)
    
    internal init(MAWBReader: @escaping (String?) -> FutureResult<[MAWBModel]>) {
        self.readMAWBs = MAWBReader
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let readMAWBs = client.readMAWBsByInbound
        self.init(MAWBReader: readMAWBs)
    }
    
    public func execute(with inboundID: String?) -> FutureResult<[MAWBScreenModel]> {
        let readMAWBs = self.readMAWBs(inboundID)
            .pipe(into: self.convertToScreenModels(dataModels:))
        return readMAWBs
    }
    
    private func convertToScreenModels(dataModels: [MAWBModel]) -> FutureResult<[MAWBScreenModel]> {
        let deferred = DeferredResult<[MAWBScreenModel]>()
        let screenModels = dataModels.map { MAWBScreenModel(dataModel: $0) }
        let sortedScreenModels = screenModels.sorted { $0.submittedTimeValue.toDate() > $1.submittedTimeValue.toDate() }
        deferred.success(value: sortedScreenModels)
        
        return deferred
    }
}
