//
//  ReadRacksCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/15/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

public class ReadRacksCommand {
    
    private var readRacks: (() -> FutureResult<[RackModel]>)
    
    internal init(rackReader: @escaping () -> FutureResult<[RackModel]>) {
        self.readRacks = rackReader
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let readRacks = client.readRacks
        self.init(rackReader: readRacks)
    }
    
    public func execute() -> FutureResult<[RackScreenModel]> {
        let readRacks = self.readRacks()
        .pipe(into: self.convertToScreenModels(dataModels:))
        return readRacks
    }
    
    private func convertToScreenModels(dataModels: [RackModel]) -> FutureResult<[RackScreenModel]> {
        let deferred = DeferredResult<[RackScreenModel]>()
        let screenModels = dataModels.map { RackScreenModel(dataModel: $0) }
        let sortedScreenModels = screenModels.sorted { $0.nameValue < $1.nameValue }
        deferred.success(value: sortedScreenModels)
        
        return deferred
    }
}
