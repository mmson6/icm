//
//  SubmitPalletEntryCommand.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation


public class SubmitPalletEntryCommand {
    
    private let submitPalletEntry: ((PalletEntryDomainModel) -> FutureResult<Bool>)
    
    internal init(palletEntrySubmit: @escaping (PalletEntryDomainModel) -> FutureResult<Bool>) {
        self.submitPalletEntry = palletEntrySubmit
    }
    
    public convenience init() {
        let client = NetworkICMClient.shared
        let entrySubmitter = client.submitPalletEntry
        self.init(palletEntrySubmit: entrySubmitter)
    }
    
    public func execute(dataModel: PalletEntryModel) -> FutureResult<Bool> {
        let submitEntry = self.convertToDomainModel(dataModel: dataModel)
        .pipe(into: self.submitPalletEntry)
        return submitEntry
    }
    
    private func convertToDomainModel(dataModel: PalletEntryModel) -> FutureResult<PalletEntryDomainModel> {
        let deferred = DeferredResult<PalletEntryDomainModel>()
        let domainModel = PalletEntryDomainModel(dataModel: dataModel)
        deferred.success(value: domainModel)
        return deferred
    }
}
