//
//  ICMError.swift
//  ICM
//
//  Created by Mikael Son on 2/14/18.
//  Copyright © 2018 MSC. All rights reserved.
//

import Foundation

enum ICMError: Error {
    case invalidServerResponse
    case requestCouldNotBeCompleted
}
